
#include <stdlib.h>
#include <fstream>

#include <mistral_solver.hpp>
#include <mistral_variable.hpp>
#include <mistral_search.hpp>

using namespace std;
using namespace Mistral;

class MCSCmdLine : public SolverCmdLine {

private:
    
	TCLAP::ValueArg<std::string> *formatArg;
	TCLAP::ValueArg<std::string> *problemArg;
	TCLAP::ValueArg<int>         *failArg;
	TCLAP::SwitchArg             *backjumpArg;
	TCLAP::SwitchArg             *dominanceArg;
	TCLAP::SwitchArg             *ignorelabelArg;
	TCLAP::SwitchArg             *connectedArg;
	TCLAP::ValueArg<std::string> *vcheuristicArg;
	TCLAP::ValueArg<int>         *nArg;
	TCLAP::ValueArg<int>         *mArg;
	TCLAP::ValueArg<int>         *witnessArg;
	TCLAP::ValueArg<int>         *filteringArg;
	TCLAP::SwitchArg             *increasingArg;
	TCLAP::ValueArg<std::string> *fileBArg;
	
	TCLAP::ValueArg<std::string> *weightArg;
	TCLAP::ValueArg<std::string> *weightcArg;
	TCLAP::ValueArg<std::string> *guideArg;
	TCLAP::ValueArg<std::string> *branchingrestArg;
	TCLAP::ValueArg<std::string> *lbArg;
	
	TCLAP::SwitchArg             *basicmodelArg;
	TCLAP::SwitchArg             *useedgesArg;
	TCLAP::SwitchArg             *decompArg;
		
	TCLAP::ValuesConstraint<std::string> *bo_allowed;
	TCLAP::ValuesConstraint<std::string> *fo_allowed;
	TCLAP::ValuesConstraint<std::string> *po_allowed;
	
	TCLAP::ValuesConstraint<std::string> *wo_allowed;
	TCLAP::ValuesConstraint<std::string> *wco_allowed;
	TCLAP::ValuesConstraint<std::string> *go_allowed;
	TCLAP::ValuesConstraint<std::string> *bro_allowed;
	TCLAP::ValuesConstraint<std::string> *lb_allowed;
    
public:
    
	MCSCmdLine(const std::string& message,
	const char delimiter = ' ',
	const std::string& version = "none",
	bool helpAndVersion = true);
	~MCSCmdLine();
	void initialise();
	std::string get_ordering();
	std::string get_branching();
	std::string get_restart();
	std::string get_vcheuristic();
	std::string get_filename();
	std::string get_filenameB();	
	std::string get_format();	
	std::string get_problem();	
	std::string get_problem_full();	
	int get_seed();	
	double get_tcutoff();
	int get_ncutoff();
	int get_filtering();
	int get_randomization(); 
	int get_verbosity(); 
	bool get_backjump();
	bool get_dominance();
	bool get_connected();
	bool get_ignorelabel();
	bool print_solution();
	bool print_parameters();
	bool print_instance();
	bool print_statistics();
	int get_n();
	int get_m();
	int get_witness();
	bool increasing();
	bool basicmodel();
	bool useedges();
	bool get_decomp();
	std::string get_weight();
	std::string get_weightc();
	std::string get_guide();
	std::string get_branching_restrictions();
	std::string get_lb_rules();
};


MCSCmdLine::MCSCmdLine(const std::string& message,
const char delimiter,
const std::string& version,
bool helpAndVersion)
: SolverCmdLine(message, delimiter, version, helpAndVersion) {
	initialise();
}
    
MCSCmdLine::~MCSCmdLine() {

	delete failArg;
	delete backjumpArg;
	delete dominanceArg;
	delete ignorelabelArg;
	delete connectedArg;
	delete vcheuristicArg;
	delete formatArg;
	delete problemArg;
	delete nArg;
	delete mArg;
	delete witnessArg;
	delete filteringArg;
	delete fo_allowed;
	delete po_allowed;
	delete bo_allowed;
	delete fileBArg;
	delete increasingArg;
	delete weightArg;
	delete weightcArg;
	delete guideArg;
	delete branchingrestArg;
	delete lbArg;
	delete basicmodelArg;
	delete useedgesArg;
	delete decompArg;

}

void MCSCmdLine::initialise() {
	
	// FORMAT 
	std::vector<std::string> foallowed;
	foallowed.push_back("clq");
	foallowed.push_back("mis");
	foallowed.push_back("mtx");
	fo_allowed = new TCLAP::ValuesConstraint<std::string>( foallowed );
	formatArg = new TCLAP::ValueArg<std::string>("","format","format",false,"mis",fo_allowed);
	add( *formatArg );
	
	// PROBLEM 
	std::vector<std::string> poallowed;
	poallowed.push_back("clq");
	poallowed.push_back("mis");
	po_allowed = new TCLAP::ValuesConstraint<std::string>( poallowed );
	problemArg = new TCLAP::ValueArg<std::string>("p","problem","problem type",false,"mis",po_allowed);
	add( *problemArg );
	
	// WEIGHT POLICY FOR VCOVER
	std::vector<std::string> woallowed;
	woallowed.push_back("none");
	woallowed.push_back("expl");
	woallowed.push_back("inwitness");
	woallowed.push_back("inwitness_geom");
	woallowed.push_back("outwitness");
	wo_allowed = new TCLAP::ValuesConstraint<std::string>( woallowed );
	weightArg = new TCLAP::ValueArg<std::string>("","weight","VC weight policy",false,"none",wo_allowed);
	add( *weightArg );
	
	// WEIGHT POLICY FOR CONNECTED
	std::vector<std::string> wcoallowed;
	wcoallowed.push_back("none");
	wcoallowed.push_back("articulation");
	wcoallowed.push_back("cut");
	wcoallowed.push_back("both");
	wco_allowed = new TCLAP::ValuesConstraint<std::string>( wcoallowed );
	weightcArg = new TCLAP::ValueArg<std::string>("","weightc","Connected weight policy",false,"both",wco_allowed);
	add( *weightcArg );

	// BRANCHING POLICY
	std::vector<std::string> broallowed;
	broallowed.push_back("all");
	broallowed.push_back("inwitness");
	broallowed.push_back("outwitness");
	broallowed.push_back("mindegree");
	broallowed.push_back("maxdegree");
	bro_allowed = new TCLAP::ValuesConstraint<std::string>( broallowed );
	branchingrestArg = new TCLAP::ValueArg<std::string>("","branch_on","branching restrictions",false,"all",bro_allowed);
	add( *branchingrestArg );

	
	// GUIDING POLICY
	std::vector<std::string> goallowed;
	goallowed.push_back("none");
	goallowed.push_back("witness");
	go_allowed = new TCLAP::ValuesConstraint<std::string>( goallowed );
	guideArg = new TCLAP::ValueArg<std::string>("","guide","VC guiding policy",false,"none",go_allowed);
	add( *guideArg );
	
	// LB POLICY
	std::vector<std::string> lballowed;
	lballowed.push_back("none");
	lballowed.push_back("NT");
	lballowed.push_back("NTCK");
	lballowed.push_back("CK");
	lb_allowed = new TCLAP::ValuesConstraint<std::string>( lballowed );
	lbArg = new TCLAP::ValueArg<std::string>("","lbkernel","kernelization rules for lower bounds",false,"none",lb_allowed);
	add( *lbArg );
	
	// Second graph
	fileBArg = new TCLAP::ValueArg<std::string>("","fileb","input file B",true,"","string");
	add( *fileBArg );
	
	// FAIL LIMIT
	failArg = new TCLAP::ValueArg<int>("","fail_limit","fail limit",false,-1,"int");
	add( *failArg );
	
	// TEST DIFFERENT MODES FOR THE CONSTRAINT FROM THE COMMAND LINE
	witnessArg = new TCLAP::ValueArg<int>("","witness","keeps a feasible witness",false,100,"int");
	add( *witnessArg );
	
	filteringArg = new TCLAP::ValueArg<int>("","filtering","filtering methods",false,1,"int");
	add( *filteringArg );
	
	// NUM NODES (RANDOM GRAPH)
	nArg = new TCLAP::ValueArg<int>("n","n","num nodes",false,10,"int");
	add( *nArg );
	
	// NUM EDGES (RANDOM GRAPH)
	mArg = new TCLAP::ValueArg<int>("m","m","num edges",false,15,"int");
	add( *mArg );
    
	// USE CLAUSE LEARNING
	backjumpArg = new TCLAP::SwitchArg("","backjump","Switch on backjump", false);
	add( *backjumpArg );
	
	// ORDER NODES BY INCREASING DEGREE
	increasingArg = new TCLAP::SwitchArg("","increasing","Switch on static var order", false);
	add( *increasingArg );
	
	// ONLY CONNECTED SUBGRAPHS
	connectedArg = new TCLAP::SwitchArg("","connected","Connected MCS only", false);
	add( *connectedArg );
	
	// ONLY CONNECTED SUBGRAPHS
	ignorelabelArg = new TCLAP::SwitchArg("","ignore-labels","Ignore all labels", false);
	add( *ignorelabelArg );
	
	// USE DOMINANCE
	dominanceArg = new TCLAP::SwitchArg("","dominance","Switch on dominance detection", true);
	add( *dominanceArg );
	
	// DO NOT USE IMPLIED CONSTRAINTS ON NEIGHBORHOOD
	basicmodelArg = new TCLAP::SwitchArg("","basic-model","Don't add extra neighbourhood constraints", false);
	add( *basicmodelArg );
	
	// USE EDGE INFORMATION
	useedgesArg = new TCLAP::SwitchArg("","edge","Dynamic edge addition", false);
	add( *useedgesArg );
	
	// WHETHER WE USE A DECOMPOSITION
	decompArg = new TCLAP::SwitchArg("","decomposition","Switch on decomposition", false);
	add( *decompArg );

	// BRANCHING
	std::vector<std::string> boallowed;
	boallowed.push_back("rem_mindegree");
	boallowed.push_back("add_maxdegree");

	bo_allowed = new TCLAP::ValuesConstraint<std::string>( boallowed );
	vcheuristicArg = new TCLAP::ValueArg<std::string>("","vcheuristic","heuristic for the brute-force vc algorithm",false,"add_maxdegree",bo_allowed);
	add( *vcheuristicArg );    

}


std::string MCSCmdLine::get_vcheuristic() {
	return vcheuristicArg->getValue();
}

std::string MCSCmdLine::get_ordering() {
	return orderingArg->getValue();
}

std::string MCSCmdLine::get_branching() {
	return branchingArg->getValue();
}

std::string MCSCmdLine::get_restart() {
	return restartArg->getValue();
}

std::string MCSCmdLine::get_filename() {
	return fileArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_filenameB() {
	return fileBArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_format() {
	return formatArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_problem() {
	return problemArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_weight() {
	return weightArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_weightc() {
	return weightcArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_guide() {
	return guideArg->getValue(); //.c_str();
}

std::string MCSCmdLine::get_branching_restrictions() {
	return branchingrestArg->getValue();
}

std::string MCSCmdLine::get_lb_rules() {
	return lbArg->getValue();
}

std::string MCSCmdLine::get_problem_full() {
	if(get_problem()=="mis") return "Maximum Independent Set"; 
	if(get_problem()=="clq" ) return "Maximum Clique"; 
	return "Unknown problem (using Maximum Independent Set)";
}

int MCSCmdLine::get_seed() { 
	return seedArg->getValue();
}

int MCSCmdLine::get_m() { 
	return mArg->getValue();
}

int MCSCmdLine::get_n() { 
	return nArg->getValue();
}

int MCSCmdLine::get_witness() { 
	return witnessArg->getValue();
}

int MCSCmdLine::get_filtering() {
	return filteringArg->getValue();
}

double MCSCmdLine::get_tcutoff() { 
	return timeArg->getValue();
}

int MCSCmdLine::get_ncutoff() { 
	return failArg->getValue();
}

int MCSCmdLine::get_randomization() { 
	return randomizationArg->getValue();
}

int MCSCmdLine::get_verbosity() { 
	return verbosityArg->getValue();
}

bool MCSCmdLine::get_backjump() { 
	return backjumpArg->getValue();
}

bool MCSCmdLine::get_connected() { 
	return connectedArg->getValue();
}

bool MCSCmdLine::get_ignorelabel() { 
	return ignorelabelArg->getValue();
}

bool MCSCmdLine::get_dominance() { 
	return dominanceArg->getValue();
}

bool MCSCmdLine::print_solution() {
	return printsolArg->getValue();
}

bool MCSCmdLine::print_parameters() {
	return printparArg->getValue();
}

bool MCSCmdLine::print_statistics() {
	return printstaArg->getValue();
}

bool MCSCmdLine::increasing() {
	return increasingArg->getValue();
}

bool MCSCmdLine::basicmodel() {
	return basicmodelArg->getValue();
}

bool MCSCmdLine::useedges() {
	return useedgesArg->getValue();
}

bool MCSCmdLine::get_decomp() {
	return decompArg->getValue();
}

//#define _DEBUG_HEURISTIC true

class SubisoHeuristic : public BranchingHeuristic, public ConflictCountManager {

public:
	

	VarStack<Variable, ReversibleNum<int> > vars[2];
	 
	int size; 
	int **mapping;
	
	int *vindex[2];
	
	int ncalls;
	

	//SubisoHeuristic() {}
	SubisoHeuristic(Solver *s, VarArray& X, VarArray& MA, VarArray& MB) : BranchingHeuristic(s), ConflictCountManager(s) {
		ncalls = 0;
		
		
		size = MA.size;
		int numvars = BranchingHeuristic::solver->variables.size;
		
		vars[0].initialise(BranchingHeuristic::solver);
		vars[0].initialise(numvars);
		
		vars[1].initialise(BranchingHeuristic::solver);
		vars[1].initialise(numvars);
		
		
		vindex[0] = new int[size];
		vindex[1] = new int[size];
		
		mapping = new int*[numvars];
		std::fill(mapping, mapping+numvars, (int*)NULL);
		
		
		for(int i=0; i<X.size; ++i) {
			int idx = X[i].id();
			
			mapping[idx] = new int[2];
			mapping[idx][0] = MA[i/size].id();
			mapping[idx][1] = MB[i%size].id();
		}
		
		
		
		int idx;
		for(int i=0; i<size; ++i) {
			idx = MA[i].id();
			vindex[0][i] = idx;
			vars[0].declare(BranchingHeuristic::solver->variables[idx]);
			mapping[idx] = new int[size];
			
			for(int j=0; j<size; ++j) {
				mapping[idx][j] = X[i*size+j].id();
			}
		}
	
		for(int i=0; i<size; ++i) {
			idx = MB[i].id();
			vindex[1][i] = idx;
			vars[1].declare(BranchingHeuristic::solver->variables[idx]);
			mapping[idx] = new int[size];

			for(int j=0; j<size; ++j) {
				mapping[idx][j] = X[j*size+i].id();
			}
		}

	}
	
	virtual ~SubisoHeuristic() {
		for(int i=0; i<BranchingHeuristic::solver->variables.size; ++i) {
			delete [] mapping[i];
		}
		delete [] mapping;
		delete [] vindex[0];
		delete [] vindex[1];
	}

	//virtual void initialise() {}
	virtual void initialise(VarStack< Variable, ReversibleNum<int> >& seq) {}
	virtual void close() {}


	void print_matrix() {
		Solver *s = BranchingHeuristic::solver;
		int ida, idb, idx;
		std::cout << "  ";
		for(int i=0; i<size; ++i) {
			idb = vindex[1][i];
			if(s->variables[idb].is_ground()) {
				std::cout << (s->variables[idb].get_min() ? "x" : " ");
			} else {
				std::cout << ".";
			}
		}
		std::cout << std::endl;
		for(int i=0; i<size; ++i) {
			std::cout << ((i+1)%(10));
			ida = vindex[0][i];
			if(s->variables[ida].is_ground()) {
				std::cout << (s->variables[ida].get_min() ? "x" : " ");
			} else {
				std::cout << ".";
			}
			for(int j=0; j<size; ++j) {
				idx = mapping[ida][j];
				if(s->variables[idx].is_ground()) {
					std::cout << (s->variables[idx].get_min() ? " " : "x");
				} else {
					std::cout << ".";
				}
			}
			std::cout << std::endl;
		}
	}

	virtual Decision branch() {
		
		// weight on Avars/Bvars: how much that node should be matched in order to not disconnect the graph
		
		// weight on X: how much that node is in big cliques (outwitness)
		//              how much that node is not in big cliques (inwitness)
		
		Variable node;
		double w[2], wX, wt, we, best_weight, best_weight_edge;
		int nm, best_nummatch, idS, idX, best_id=-1, best_idX;
		Variable best_var, x;

#ifdef _DEBUG_HEURISTIC
		if(_DEBUG_HEURISTIC) {
			print_matrix();
		}
#endif

		int side = 0;		
		
		do {
			for(int i=vars[side].size-1; i>=0; --i) {
				node = vars[side][i];
			
#ifdef _DEBUG_HEURISTIC
				if(_DEBUG_HEURISTIC) {
					std::cout << " - " << node << ":";
				}
#endif
		
				if(node.is_ground()) {

#ifdef _DEBUG_HEURISTIC
					if(_DEBUG_HEURISTIC) {				
						std::cout << "(g)" ;
					}
#endif
								
					vars[side].remove(node);
				} else { 
					idS = node.id();
					w[side] = variable_weight[idS];
				
				
					nm = 0;
					wX = 0;
					w[1-side] = 0;
				
					best_idX = -1;
					for(int j=0; j<size; ++j) {
						idX = mapping[idS][j];
						assert(mapping[idX][side] == idS);
					
						if(!BranchingHeuristic::solver->variables[idX].is_ground()) {
							++nm;
							we = w[side] + variable_weight[idX] + variable_weight[mapping[idX][1-side]];
						
							wX += variable_weight[idX];
						
							w[1-side] += variable_weight[mapping[idX][1-side]];
						
#ifdef _DEBUG_HEURISTIC
							if(_DEBUG_HEURISTIC) {				
								std::cout << " " << variable_weight[idX] << "/" << variable_weight[mapping[idX][1]];
							}
#endif
						
							if(best_idX<0 || we>best_weight_edge) {
								best_idX = idX;
								best_weight_edge = we;
							}
						
						} else {
							assert(BranchingHeuristic::solver->variables[idX].get_min()==1);
						}
					}
				
					w[side] *= (double)nm;

					wt = w[0] + wX + w[1];

#ifdef _DEBUG_HEURISTIC
					if(_DEBUG_HEURISTIC) {								
						std::cout << "[" << nm << ": " << w[side] << "|" << wX << "|" << w[1-side] << "] -> "
							<< BranchingHeuristic::solver->variables[best_idX] << " in "
								<< BranchingHeuristic::solver->variables[best_idX].get_domain();
					}
#endif
				
					//
					if(best_id<0 || (best_weight*(double)nm < wt*(double)best_nummatch)) {
						best_id = idS;
						best_nummatch = nm;
						best_weight = wt;
						best_var = BranchingHeuristic::solver->variables[best_idX];
						
#ifdef _DEBUG_HEURISTIC
						if(_DEBUG_HEURISTIC) {				
							std::cout << "*" << std::endl;
						}
#endif
					}
#ifdef _DEBUG_HEURISTIC			
					else if(_DEBUG_HEURISTIC) { 
						std::cout << std::endl;
					}
#endif
				
				}
			
			}
			
#ifdef _DEBUG_HEURISTIC
			if(_DEBUG_HEURISTIC) {				
				std::cout << std::endl;
			}
#endif
		
		} while(++side<2 && best_id);


#ifdef _DEBUG_HEURISTIC
		if(_DEBUG_HEURISTIC) {			
			if(best_id<0) {
		
				std::cout << "vars[" << side << "] is empty" << std::endl;
				exit(1);
			}
		}
#endif
		
		Solver *s = BranchingHeuristic::solver;
		if(best_id<0) {
			best_var = BranchingHeuristic::solver->sequence[0];
			best_weight = variable_weight[best_var.id()];
			for(int i=1; i<BranchingHeuristic::solver->sequence.size; ++i) {
				x = BranchingHeuristic::solver->sequence[i];
				wt = variable_weight[x.id()];
				// C : we now take into account guiding restrictions (e.g. "branch on variables outside the witness")
				if ((!s->preferredvars.contain(best_var.id()) && s->preferredvars.contain(x.id())) || ((s->preferredvars.contain(best_var.id()) == s->preferredvars.contain(x.id())) && (wt > best_weight))) {
					best_var = x;
					best_weight = wt;
				}
			}
		}
		

		Decision best_branch(best_var, Decision::ASSIGNMENT, 0);
		
#ifdef _DEBUG_HEURISTIC
		if(_DEBUG_HEURISTIC) {		
			std::cout << " ==> " << best_branch << "(" << best_var.get_domain() << ")" << std::endl << std::endl;
		}
#endif
		
		return best_branch;
	}

	virtual std::ostream& display(std::ostream& os) {
		os << "dedicated heuristic for maximum common subgraph isomorphism";
		return os;
	}

};




unsigned short read_word(FILE *in) { 
	
	unsigned char b1, b2;
	
	b1=getc(in); /* Least-significant Byte */
	b2=getc(in); /* Most-significant Byte */
	
	return b1 | (b2 << 8);
}

void read_conte_unlabeled(const std::string filename, CompactGraph& G) {

	FILE* infile = fopen(filename.c_str(), "r");
	assert(infile != NULL);
	
	int nodes, edges, target;
	
	nodes = read_word(infile);
	
	G.initialise(nodes);
	
	/* For each node i ... */
	for(int i=0; i<nodes; i++) { 
	
        /* Read the number of edges coming out of node i */
        edges=read_word(infile);
		
        	/* For each edge out of node i... */
        	for(int j=0; j<edges; j++) { 
            		/* Read the destination node of the edge */
            		target = read_word(infile);
			
            		/* Insert the edge */
            		G.add_undirected(i,target); // maybe -1 on both arguments ?
		}
	}
}

std::pair<int*, int**> read_conte_labeled(const std::string filename, CompactGraph& G) {

	FILE* infile = fopen(filename.c_str(), "r");
	assert(infile != NULL);

	int nodes, edges, target, dump;
	
	nodes = read_word(infile);
	
	int* node_attr = new int[nodes];
	int** edge_attr = new int*[nodes];
	
	G.initialise(nodes);

	for(int i=0; i<nodes; i++) { 
		edge_attr[i] = new int[nodes];
		node_attr[i] = read_word(infile);
	}

	/* For each node i... */
	for(int i=0; i<nodes; i++) {
	
		/* Read the number of edges coming out of node i */
		edges=read_word(infile);
		
		/* For each edge out of node i... */
		for(int j=0; j<edges; j++) {
		
			/* Read the destination node of the edge */
			target = read_word(infile);
			
			if (!G.matrix[i].contain(target)) {
			
				/* Insert the edge */
				G.add_undirected(i,target);
			
				/* Read the edge attribute and insert in the matrix that holds the attributes of edges */
				edge_attr[i][target] = read_word(infile);
				edge_attr[target][i] = edge_attr[i][target]; // for symmetry
			} else {
				dump = read_word(infile);
			}
		}
	}
	
	return make_pair(node_attr,edge_attr);
}

int main(int argc, char **argv)
{
	
	MCSCmdLine cmd("VertexCover", ' ', "0.0");
	cmd.parse(argc, argv);
	
	if(cmd.get_backjump()) {
		std::cout << "I have changed the default to be no backjump, please run without the '--backjump' option" << std::endl;
		exit(0);
	}
	
	
	if(cmd.print_parameters()) {
		std::cout << " c graph A    = " << cmd.get_filename() << std::endl;
		std::cout << " c graph B    = " << cmd.get_filenameB() << std::endl;
		std::cout << " c labels     = " << (cmd.get_ignorelabel() ? "yes" : "no") << std::endl;
		std::cout << " c connected  = " << (cmd.get_connected() ? "yes" : "no") << std::endl;
		std::cout << " c problem    = " << cmd.get_problem_full() << std::endl;
		std::cout << " c format     = " << cmd.get_format() << std::endl;
		std::cout << " c model      = " << (cmd.basicmodel() ? "basic" : "with neighbourhood cardinality constraints") 
																		<< (cmd.useedges() ? "+ dynamic edges" : "") 
																		<< std::endl;
		std::cout << " c vc branch  = " << cmd.get_vcheuristic() << std::endl;
		std::cout << " c seed       = " << cmd.get_seed() << std::endl;
		std::cout << " c weigh      = " << cmd.get_weight() << "/" << cmd.get_weightc() << std::endl;
		std::cout << " c guiding    = " << cmd.get_guide() << std::endl;
		std::cout << " c branching  = " << cmd.get_branching_restrictions() << std::endl;
		std::cout << " c lb kernel  = " << cmd.get_lb_rules() << std::endl;
		std::cout << " c cutoff     = time: " ;
		if(cmd.get_tcutoff()>0) {
			std::cout << cmd.get_tcutoff() << " s";
		} else {
			std::cout << "none";
		}
		std::cout << " / search: ";
		if(cmd.get_ncutoff()>0) {
			std::cout << cmd.get_ncutoff() << " fails";
		} else {
			std::cout << "none";
		}
		std::cout << std::endl;
		std::cout << " c randomized = " << (cmd.get_randomization()>0 ? "yes" : "no") << std::endl;
		std::cout << " c restarts   = " << cmd.get_restart() << std::endl;
		std::cout << " c backjumps  = " << (cmd.get_backjump() ? "yes" : "no") << std::endl;
		std::cout << " c dominance  = " << (cmd.get_dominance() ? "yes" : "no") << std::endl;
		std::cout << " c filtering  = ";
		if (cmd.get_filtering() & 1) {
			std::cout << "rigid crowns";
			if ((cmd.get_filtering() >> 1) & 1) std::cout << " + Emmanuel's rule";
			if ((cmd.get_filtering() >> 2) & 1) std::cout << " + high degree pruning";
		} else if ((cmd.get_filtering() >> 1) & 1) {
			std::cout << "Emmanuel's rule";
			if ((cmd.get_filtering() >> 2) & 1) std::cout << " + high degree pruning";
		} else if ((cmd.get_filtering() >> 2) & 1) {
			std::cout << "high degree pruning";
		} else {
			std::cout << "nothing";
		}
		std::cout << std::endl;
		std::cout << " c strategy   = " << cmd.get_ordering() << " " << cmd.get_branching() << "(" << cmd.get_randomization() << ")" << std::endl;		
	}

	usrand(cmd.get_seed());

	CompactGraph GA;
	CompactGraph GB;
	
	std::pair<int*, int**> pa = read_conte_labeled(cmd.get_filename(), GA);
	std::pair<int*, int**> pb = read_conte_labeled(cmd.get_filenameB(), GB);
	
	// Generate the compatibility graph
	
	CompactGraph G;
	G.initialise(GA.capacity*GB.capacity);
	
	int v,w,vp,wp;
	for (int i=0; i<G.node.size; i++) {
		for (int j=i+1; j<G.node.size; j++) {
			// i : v <-> w
			// j : vp <-> wp
			w = i % GB.node.size;
			v = (i - w)/GB.node.size;
			wp = j % GB.node.size;
			vp = (j - wp)/GB.node.size;
			
			/* We add an edge if the two assignments are *incompatible* */
			
			// Reason 1 : v = vp or w = wp
			if ((v == vp) || (w == wp)) {
				G.add_undirected(i,j);
				continue;
			}
			
			if (!cmd.get_ignorelabel()) {
				// Reason 2 : l(v) != l(w) or l(vp) != l(wp)
				if ((pa.first[v] != pb.first[w]) || (pa.first[vp] != pb.first[wp])) {
					G.add_undirected(i,j);
					continue;
				}
			}
			
			// Reason 3 : there is one edge (v,vp) but no edge (w,wp), or vice-versa
			if ((GA.matrix[v].contain(vp) && !GB.matrix[w].contain(wp)) || (!GA.matrix[v].contain(vp) && GB.matrix[w].contain(wp))) {
				G.add_undirected(i,j);
				continue;
			}
			
			if (!cmd.get_ignorelabel()) {
				// Reason 4 : the labels on the edges (v,p) and (w,wp) are distinct
				if (GA.matrix[v].contain(vp) && (pa.second[v][vp] != pb.second[w][wp])) {
					G.add_undirected(i,j);
					continue;
				}
			}
			
			// If none of the above, the pair is compatible.
		}
	}

	// Create CP model
	Solver solver;
	
	VarArray X(G.size());
	Variable K(0, G.size());
	
	int guide = 0;
	if (!strcmp(cmd.get_guide().c_str(),"witness")) guide = 1;
	if (cmd.get_witness() == 0 || cmd.get_guide() == "none") {
		if (cmd.get_witness() == 0) std::cout << " " << solver.parameters.prefix_comment << " No witness. Ignoring guiding policy and defaulting to \"none\"" << std::endl;
		guide = 0;
	}
	
	int weight = 0;
	if (!strcmp(cmd.get_weight().c_str(),"inwitness")) weight = 1;
	if (!strcmp(cmd.get_weight().c_str(),"inwitness_geom")) weight = 3;
	if (!strcmp(cmd.get_weight().c_str(),"outwitness")) weight = 2;
	if (!strcmp(cmd.get_weight().c_str(),"expl")) weight = 0;
	if (!strcmp(cmd.get_weight().c_str(),"none")) weight = -1;
	if (cmd.get_witness() == 0 && (weight != -1)) {
		std::cout << " " << solver.parameters.prefix_comment << " No witness. Ignoring weight policy and defaulting to \"none\"" << std::endl;
		weight = -1;
	}
	
	int weightc = 0;
	if (!strcmp(cmd.get_weightc().c_str(),"articulation")) weightc = 1;
	if (!strcmp(cmd.get_weightc().c_str(),"cut")) weightc = 2;
	if (!strcmp(cmd.get_weightc().c_str(),"both")) weightc = 3;
	
	int branching = 0;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"inwitness")) branching = 1;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"outwitness")) branching = 2;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"mindegree")) branching = 3;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"maxdegree")) branching = 4;
	if (((cmd.get_witness() == 0) && (branching == 1 || branching == 2)) || cmd.get_branching_restrictions() == "all") {
		if (cmd.get_witness() == 0) std::cout << " " << solver.parameters.prefix_comment << " Ignoring branching restrictions and defaulting to \"all\"" << std::endl;
		branching = 0;
	}
	
	int lbrules = 0;
	if (!strcmp(cmd.get_lb_rules().c_str(),"CK")) lbrules = 1;
	if (!strcmp(cmd.get_lb_rules().c_str(),"NT")) lbrules = 2;
	if (!strcmp(cmd.get_lb_rules().c_str(),"NTCK")) lbrules = 3;
	
	bool add_maxdeg = !strcmp(cmd.get_vcheuristic().c_str(), "add_maxdegree");
	
	VarArray Y;
	
	// Add the clique constraint on the compatibility graph
	if(!cmd.basicmodel() && cmd.useedges()) {
		Y = VarArray((G.size()-1)*G.size()/2);
		solver.add(VertexCover(X,Y,G,cmd.get_witness(),cmd.get_filtering(),cmd.get_backjump(),cmd.get_dominance(),weight,guide,branching,add_maxdeg,lbrules,cmd.get_decomp()) == K);
	} else {
		solver.add(VertexCover(X,G,cmd.get_witness(),cmd.get_filtering(),cmd.get_backjump(),cmd.get_dominance(),weight,guide,branching,add_maxdeg,lbrules,cmd.get_decomp()) == K);
	}
	solver.minimize(K);
	
	VarArray MA(GA.size());
	VarArray MB(GB.size());
	
	VarArray L;
	if (cmd.get_connected()) {
	
		// Add the connectivity constraint on GA
		for (int i=0; i<GA.size(); i++) {
			L.clear();
			L.add(MA[i]);
			
			for (int j=0; j<GB.size(); j++) {
				L.add(X[i*GB.size()+j]);
			}
			solver.add(BoolSum(L,GB.size(),GB.size()));			
			
		}
		solver.add(Connected(MA,GA,weightc));
		
		// Add the connectivity constraint on GB
		for (int i=0; i<GB.size(); i++) {
			L.clear();
			L.add(MB[i]);
			
			for (int j=0; j<GA.size(); j++) {
				L.add(X[j*GB.size()+i]);
			}
			
			solver.add(BoolSum(L,GB.size(),GB.size()));
			
		}
		solver.add(Connected(MB,GB,weightc));
		
	}



	BitSet neighborhood(0, std::max(GA.size(), GB.size()), BitSet::empt);
	if (!cmd.basicmodel()) {
	
		VarArray NSA(GA.size(),0,GA.size());
		VarArray NSB(GB.size(),0,GB.size());
	
		for (int i=0; i<GA.size(); i++) {
			L.clear();
			for (int j=0; j<GA.neighbor[i].size; j++) {
				L.add(MA[GA.neighbor[i][j]]);
			}

			//NSA.add(BoolSum(L));
			solver.add(NSA[i] == BoolSum(L));
			//solver.add(MA[i] <= NSA[i]);
		}


		for (int i=0; i<GB.size(); i++) {
			L.clear();
			for (int j=0; j<GB.neighbor[i].size; j++) {
				L.add(MB[GB.neighbor[i][j]]);
			}

			//NSB.add(BoolSum(L));
			solver.add(NSB[i] == BoolSum(L));
			//solver.add(MB[i] <= NSB[i]);
		}

		for (int i=0; i<GA.size(); i++) {
			for (int j=0; j<GB.size(); j++) {
				solver.add((NSA[i] != NSB[j]) <= X[i*GB.size()+j]);
			}
		}
		
		if(cmd.useedges()) {
			for (int i=0; i<GA.size(); i++) {
				for (int j=i+1; j<GA.size(); j++) {
					std::cout << "check the neighborhood of " << i << " and " << j << std::endl;
					std::cout << GA.neighbor[i] << " " << GA.matrix[i] << std::endl;
					std::cout << GA.neighbor[j] << " " << GA.matrix[j] << std::endl << std::endl;
				}
			}
			//std::cout << "do something" << std::endl;
			//exit(0);
		}
		
	}
	
	// Add the naive bound [K = |G|-sum(MA)]
	solver.add(BoolSum(MA) == (-K+G.size()));
	solver.add(BoolSum(MB) == (-K+G.size()));
	
	solver.consolidate();
	cmd.set_parameters(solver);
	solver.parameters.verbosity = cmd.get_verbosity();
	
	VarArray sorted;
	ReversibleCompactGraph rg(G);
	
	if(cmd.increasing()) {
		for(int d=rg.min_degree; d<=rg.max_degree; ++d) {
			if(!rg.node_of_degree[d].empty()) {
				for(int j=0; j<rg.node_of_degree[d].size; ++j) {
					sorted.add(X[rg.node_of_degree[d][j]]);
				}
			}
		}
	} else {
		for(int d=rg.max_degree; d>=rg.min_degree; --d) {
			if(!rg.node_of_degree[d].empty()) {
				for(int j=0; j<rg.node_of_degree[d].size; ++j) {
					sorted.add(X[rg.node_of_degree[d][j]]);
				}
			}
		}
	}
	
	//
	// VarArray allvars;
	// for(int i=0; i<MA.size; ++i) {
	// 	allvars.add(MA[i]);
	// }
	// for(int i=0; i<MB.size; ++i) {
	// 	allvars.add(MB[i]);
	// }
	// for(int i=0; i<sorted.size; ++i) {
	// 	allvars.add(sorted[i]);
	// }
	
	std::cout << G.num_edges << " edges / " << (G.size()-1)*(G.size())/2 << std::endl;

	
	BranchingHeuristic *branching_heuristic = NULL;
	
	if(cmd.get_ordering() == "subiso") {
		branching_heuristic = new SubisoHeuristic(&solver, X, MA, MB);
	} else {
		branching_heuristic = solver.heuristic_factory(cmd.get_ordering(), cmd.get_branching(), cmd.get_randomization());
	}
	RestartPolicy *restart_policy = solver.restart_factory(cmd.get_restart());
	solver.depth_first_search(sorted, branching_heuristic, restart_policy);
	
	std::cout << " " << solver.parameters.prefix_comment << " The two graphs have a maximum common subgraph of size " << G.node.size - K.get_solution_int_value() << std::endl;
	std::cout << " " << solver.parameters.prefix_comment << " This corresponds to a MCS that contains " << 100*(((double)G.node.size - (double)K.get_solution_int_value())/((double)GA.node.size)) << "% of the nodes" << std::endl;
	
	if(cmd.print_solution()) {
		if(solver.statistics.num_solutions) {
			std::cout << " " << solver.parameters.prefix_comment << " cover (" << K.get_solution_int_value() << "): " ;
			for(int i=0; i<G.size(); ++i) {
				if(X[i].get_solution_int_value())
					std::cout << i << " ";
			}
			std::cout << std::endl;
		} else {
			std::cout << " " << solver.parameters.prefix_comment << " no solution" << std::endl;
		}
	}
   
	if(cmd.print_statistics())
		 solver.statistics.print_full(std::cout);
		 
	// cleanup
	delete pa.first;
	delete pb.first;
	for (int i=0; i<GA.size(); i++) {
		delete pa.second[i];
	}
	delete pa.second;
	for (int i=0; i<GB.size(); i++) {
		delete pb.second[i];
	}
	delete pb.second;
}









