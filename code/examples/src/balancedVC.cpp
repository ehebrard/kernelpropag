
#include <stdlib.h>
#include <fstream>

 

#include <mistral_solver.hpp>
#include <mistral_variable.hpp>
#include <mistral_search.hpp>


using namespace std;
using namespace Mistral;



class MISCmdLine : public Mistral::SolverCmdLine {

private:
    
	TCLAP::ValueArg<std::string> *formatArg;
	TCLAP::ValueArg<std::string> *problemArg;
	TCLAP::ValueArg<int>         *failArg;
	TCLAP::SwitchArg             *backjumpArg;
	TCLAP::SwitchArg             *dominanceArg;
	TCLAP::ValueArg<std::string> *vcheuristicArg;
	TCLAP::ValueArg<int>         *nArg;
	TCLAP::ValueArg<int>         *mArg;
	TCLAP::ValueArg<int>         *witnessArg;
	TCLAP::ValueArg<int>         *npartArg;
	TCLAP::ValueArg<int>         *balanceArg;
	TCLAP::ValueArg<int>         *filteringArg;
	
	TCLAP::SwitchArg             *increasingArg;
	TCLAP::SwitchArg             *decompArg;
	
	TCLAP::ValueArg<std::string> *weightArg;
	TCLAP::ValueArg<std::string> *guideArg;
	TCLAP::ValueArg<std::string> *branchingrestArg;
	TCLAP::ValueArg<std::string> *lbArg;
		
	TCLAP::ValuesConstraint<std::string> *bo_allowed;
	TCLAP::ValuesConstraint<std::string> *fo_allowed;
	TCLAP::ValuesConstraint<std::string> *po_allowed;
	
	TCLAP::ValuesConstraint<std::string> *wo_allowed;
	TCLAP::ValuesConstraint<std::string> *go_allowed;
	TCLAP::ValuesConstraint<std::string> *bro_allowed;
	TCLAP::ValuesConstraint<std::string> *lb_allowed;
    
public:
    
	MISCmdLine(const std::string& message,
	const char delimiter = ' ',
	const std::string& version = "none",
	bool helpAndVersion = true);
    
	~MISCmdLine();

	void initialise();
	
	std::string get_ordering();

	std::string get_branching();
	
	std::string get_restart();
	
	std::string get_vcheuristic();

	std::string get_filename();
		
	std::string get_format();
		
	std::string get_problem();
	
		
	std::string get_problem_full();
		
	int get_seed() ;
		
	double get_tcutoff() ;
	int    get_ncutoff() ;
	
	int get_filtering();

	int get_randomization(); 
		
	int get_verbosity(); 
		
	bool get_backjump();
		
	bool get_dominance();
		
	bool print_solution();

	bool print_parameters()  ;

	bool print_instance()  ;

	bool print_statistics() ;
	
	bool print_model() ;
	
	int get_n() ;
	
	int get_m() ;
	
	int get_witness() ;
	
	int n_partition() ;
	
	int balance() ;
	
	bool increasing() ;
	
	bool get_decomp();
	
	std::string get_weight();
	std::string get_guide();
	std::string get_branching_restrictions();
	std::string get_lb_rules();

};


MISCmdLine::MISCmdLine(const std::string& message,
const char delimiter,
const std::string& version,
bool helpAndVersion)
: SolverCmdLine(message, delimiter, version, helpAndVersion) {
	initialise();
}
    
MISCmdLine::~MISCmdLine() {

	delete failArg;
	delete backjumpArg;
	delete dominanceArg;
	delete vcheuristicArg;
	delete formatArg;
	delete problemArg;
	delete nArg;
	delete mArg;
	delete witnessArg;
	delete npartArg;
	delete balanceArg;
	delete filteringArg;
	delete increasingArg;
	delete decompArg;
	
	delete fo_allowed;
	delete po_allowed;
	delete bo_allowed;
	
	delete weightArg;
	delete guideArg;
	delete branchingrestArg;
	delete lbArg;
	
	delete wo_allowed;
	delete go_allowed;
	delete bro_allowed;

}

void MISCmdLine::initialise() {

	// // INPUT FILE
	// fileArg = new TCLAP::ValueArg<std::string>("f","file","instance file",true,"random","string");
	// add( *fileArg );
	
	// FORMAT 
	std::vector<std::string> foallowed;
	foallowed.push_back("clq");
	foallowed.push_back("mis");
	foallowed.push_back("mtx");
	foallowed.push_back("snap");
	fo_allowed = new TCLAP::ValuesConstraint<std::string>( foallowed );
	formatArg = new TCLAP::ValueArg<std::string>("","format","format",false,"mis",fo_allowed);
	add( *formatArg );
	
	// PROBLEM 
	std::vector<std::string> poallowed;
	poallowed.push_back("clq");
	poallowed.push_back("mis");
	po_allowed = new TCLAP::ValuesConstraint<std::string>( poallowed );
	problemArg = new TCLAP::ValueArg<std::string>("p","problem","problem type",false,"mis",po_allowed);
	add( *problemArg );
	
	// WEIGHT POLICY
	std::vector<std::string> woallowed;
	woallowed.push_back("none");
	woallowed.push_back("expl");
	woallowed.push_back("inwitness");
	woallowed.push_back("outwitness");
	wo_allowed = new TCLAP::ValuesConstraint<std::string>( woallowed );
	weightArg = new TCLAP::ValueArg<std::string>("","weight","VC weight policy",false,"expl",wo_allowed);
	add( *weightArg );
	
	// BRANCHING POLICY
	std::vector<std::string> broallowed;
	broallowed.push_back("all");
	broallowed.push_back("inwitness");
	broallowed.push_back("outwitness");
	broallowed.push_back("mindegree");
	broallowed.push_back("maxdegree");
	bro_allowed = new TCLAP::ValuesConstraint<std::string>( broallowed );
	branchingrestArg = new TCLAP::ValueArg<std::string>("","branch_on","branching restrictions",false,"all",bro_allowed);
	add( *branchingrestArg );
	
	// GUIDING POLICY
	std::vector<std::string> goallowed;
	goallowed.push_back("none");
	goallowed.push_back("witness");
	go_allowed = new TCLAP::ValuesConstraint<std::string>( goallowed );
	guideArg = new TCLAP::ValueArg<std::string>("","guide","VC guiding policy",false,"none",go_allowed);
	add( *guideArg );
	
	// LB POLICY
	std::vector<std::string> lballowed;
	lballowed.push_back("none");
	lballowed.push_back("NT");
	lballowed.push_back("NTCK");
	lballowed.push_back("CK");
	lb_allowed = new TCLAP::ValuesConstraint<std::string>( lballowed );
	lbArg = new TCLAP::ValueArg<std::string>("","lbkernel","kernelization rules for lower bounds",false,"none",lb_allowed);
	add( *lbArg );
	// // TIME LIMIT
	// timeArg = new TCLAP::ValueArg<double>("t","time_limit","time limit",false,-1,"double");
	// add( *timeArg );
	
	// FAIL LIMIT
	failArg = new TCLAP::ValueArg<int>("","fail_limit","fail limit",false,-1,"int");
	add( *failArg );

	// // VERBOSITY LEVEL
	// verbosityArg = new TCLAP::ValueArg<int>("v","verbosity","verbosity level",false,1,"int");
	// add( *verbosityArg );
    
	// // RANDOM SEED
	// seedArg = new TCLAP::ValueArg<int>("s","seed","random seed",false,12345,"int");
	// add( *seedArg );
	
	// TEST DIFFERENT MODES FOR THE CONSTRAINT FROM THE COMMAND LINE
	witnessArg = new TCLAP::ValueArg<int>("","witness","keeps a feasible witness",false,100,"int");
	add( *witnessArg );
	
	// NUMBER OF PARTITIONS
	npartArg = new TCLAP::ValueArg<int>("","npartition","number of partitions",false,3,"int");
	add( *npartArg );
	
	// BALANCE SLACK
	balanceArg = new TCLAP::ValueArg<int>("","balance","maximum deviation",false,1,"int");
	add( *balanceArg );
	
	filteringArg = new TCLAP::ValueArg<int>("","filtering","filtering methods",false,1,"int");
	add( *filteringArg );
	
	// NUM NODES (RANDOM GRAPH)
	nArg = new TCLAP::ValueArg<int>("n","n","num nodes",false,10,"int");
	add( *nArg );
	
	// NUM EDGES (RANDOM GRAPH)
	mArg = new TCLAP::ValueArg<int>("m","m","num edges",false,15,"int");
	add( *mArg );
    
	// // HEURISTIC RANDOMIZATION
	// randomizationArg = new TCLAP::ValueArg<int>("z","randomization","randomization level",false,0,"int");
	// add( *randomizationArg );
    
	// // RESTART FACTOR
	// factorArg = new TCLAP::ValueArg<double>("o","factor","restart factor",false,1.05,"double");
	// add( *factorArg );
	//
	// // RESTART BASE
	// baseArg = new TCLAP::ValueArg<int>("e","base","restart base",false,0,"int");
	// add( *baseArg );
    
	// USE CLAUSE LEARNING
	backjumpArg = new TCLAP::SwitchArg("","backjump","Switch on backjump", false);
	add( *backjumpArg );
	
	// ORDER NODES BY INCREASING DEGREE
	increasingArg = new TCLAP::SwitchArg("","increasing","Switch on static var order", false);
	add( *increasingArg );
	
	// WHETHER WE USE A DECOMPOSITION
	decompArg = new TCLAP::SwitchArg("","decomposition","Switch on decomposition", false);
	add( *decompArg );
	
	// USE DOMINANCE
	dominanceArg = new TCLAP::SwitchArg("","dominance","Switch on dominance detection", true);
	add( *dominanceArg );

	// // PRINT STATISTICS
	// printstaArg = new TCLAP::SwitchArg("", "print_sta","Print the statistics", true);
	// add( *printstaArg );

	// // PRINT PARAMETERS
	// printparArg = new TCLAP::SwitchArg("", "print_par","Print the parameters", true);
	// add( *printparArg );
	//
	// // PRINT SOLUTION
	// printsolArg = new TCLAP::SwitchArg("", "print_sol", "Print the solution, if found", true);
	// add( *printsolArg );

	// BRANCHING
	std::vector<std::string> boallowed;
	boallowed.push_back("rem_mindegree");
	boallowed.push_back("add_maxdegree");

	bo_allowed = new TCLAP::ValuesConstraint<std::string>( boallowed );
	vcheuristicArg = new TCLAP::ValueArg<std::string>("","vcheuristic","heuristic for the brute-force vc algorithm",false,"add_maxdegree",bo_allowed);
	add( *vcheuristicArg );    

}


std::string MISCmdLine::get_vcheuristic() {
	return vcheuristicArg->getValue();
}

std::string MISCmdLine::get_ordering() {
	return orderingArg->getValue();
}

std::string MISCmdLine::get_branching() {
	return branchingArg->getValue();
}

std::string MISCmdLine::get_restart() {
	return restartArg->getValue();
}

std::string MISCmdLine::get_filename() {
	return fileArg->getValue(); //.c_str();
}

std::string MISCmdLine::get_format() {
	return formatArg->getValue(); //.c_str();
}

std::string MISCmdLine::get_problem() {
	return problemArg->getValue(); //.c_str();
}

std::string MISCmdLine::get_problem_full() {
	if(get_problem()=="mis") return "Maximum Independent Set"; 
	if(get_problem()=="clq" ) return "Maximum Clique"; 
	return "Unknown problem (using Maximum Independent Set)";
}

int MISCmdLine::get_seed() { 
	return seedArg->getValue();
}

int MISCmdLine::get_m() { 
	return mArg->getValue();
}

int MISCmdLine::get_n() { 
	return nArg->getValue();
}

int MISCmdLine::get_witness() { 
	return witnessArg->getValue();
}

int MISCmdLine::n_partition() { 
	return npartArg->getValue();
}

int MISCmdLine::balance() { 
	return balanceArg->getValue();
}

int MISCmdLine::get_filtering() {
	return filteringArg->getValue();
}

double MISCmdLine::get_tcutoff() { 
	return timeArg->getValue();
}

int MISCmdLine::get_ncutoff() { 
	return failArg->getValue();
}

int MISCmdLine::get_randomization() { 
	return randomizationArg->getValue();
}

int MISCmdLine::get_verbosity() { 
	return verbosityArg->getValue();
}

bool MISCmdLine::get_backjump() { 
	return backjumpArg->getValue();
}

bool MISCmdLine::get_dominance() { 
	return dominanceArg->getValue();
}

bool MISCmdLine::print_solution() {
	return printsolArg->getValue();
}

bool MISCmdLine::print_model() {
	return printmodArg->getValue();
}

bool MISCmdLine::print_parameters() {
	return printparArg->getValue();
}

bool MISCmdLine::print_statistics() {
	return printstaArg->getValue();
}

bool MISCmdLine::get_decomp() {
	return decompArg->getValue();
}

bool MISCmdLine::increasing() {
	return increasingArg->getValue();
}

std::string MISCmdLine::get_weight() {
	return weightArg->getValue(); //.c_str();
}

std::string MISCmdLine::get_guide() {
	return guideArg->getValue(); //.c_str();
}

std::string MISCmdLine::get_branching_restrictions() {
	return branchingrestArg->getValue();
}

std::string MISCmdLine::get_lb_rules() {
	return lbArg->getValue();
}

void read_snap(const std::string filename, CompactGraph& G) {
	
	std::ifstream infile(filename.c_str(), ios_base::in);
		
	string dump;

	infile >> dump;
	while(dump=="#") {
		infile.ignore(INFTY,'\n');
		infile >> dump;
	}
	
	infile >> dump;
	
	int N, M, maxval;
	
	infile >> N;
	infile >> M;
	infile >> maxval;
	
	assert(maxval <= 1000000);
	maxval++;

	G.initialise(N);
	
	std::vector<int> node;
	node.assign(maxval,-1);
	int cpt = 0;
	
	int x, y;
	for(int i=0; i<M; ++i) {
		infile >> x;
		infile >> y;
		infile.ignore(INFTY,'\n');
		
		if (x == y) continue;
		
		assert((x < maxval) && (y < maxval));
		
		if (node[x] == -1) {
			node[x] = cpt;
			cpt++;
		}
		
		if (node[y] == -1) {
			node[y] = cpt;
			cpt++;
		}
		
		assert(cpt < maxval);
		assert(node[x] != node[y]);
		
		if (G.matrix[node[x]].contain(node[y])) continue;
		
		G.add_undirected(node[x],node[y]);		
	}
}

void read_dimacs(const std::string filename, Graph& G, const bool complement=false) {
	
	std::ifstream infile(filename.c_str(), ios_base::in);
		
	string dump;

	infile >> dump;
	while(dump=="c") {
		infile.ignore(INFTY,'\n');
		infile >> dump;
	}
	
	infile >> dump;
	
	int N, M;
	
	infile >> N;
	infile >> M;

	G.initialise(N, true, complement);
	
	
	int x, y;
	for(int i=0; i<M; ++i) {
		infile >> dump;
		
		while(dump=="c") {
			infile.ignore(INFTY,'\n');
			infile >> dump;
		}
		
		infile >> x;
		infile >> y;

		
		infile.ignore(INFTY,'\n');
		
		assert(x != y);
		
		if(complement)
			G.rem_undirected(x-1,y-1);		
		else 
			G.add_undirected(x-1,y-1);		
	}
}


void read_mtx(const std::string filename, Graph& G, const bool complement=false) {
	std::ifstream infile(filename.c_str(), ios_base::in);
		
	string dump;
	
	infile >> dump;
		
	while(dump[0]=='\%') {
		infile.ignore(INFTY,'\n');
		infile >> dump;
	}
	
	int N, M;
	
	infile >> N;
	infile >> M;
		
	G.initialise(N, true, complement);
	
	int x, y;
	for(int i=0; i<M; ++i) {

		infile >> x;
		infile >> y;		
		infile.ignore(INFTY,'\n');
		
		assert(x != y);
				
		if(complement)
			G.rem_undirected(x-1,y-1);		
		else
			G.add_undirected(x-1,y-1);	
			
	}
}



void read_dimacs(const std::string filename, CompactGraph& G, const bool complement=false) {
	
	std::ifstream infile(filename.c_str(), ios_base::in);
		
	string dump;

	infile >> dump;
	while(dump=="c") {
		infile.ignore(INFTY,'\n');
		infile >> dump;
	}
	
	infile >> dump;
	
	int N, M;
	
	infile >> N;
	infile >> M;

	G.initialise(N);
	
	
	int x, y;
	for(int i=0; i<M; ++i) {
		infile >> dump;
		
		while(dump=="c") {
			infile.ignore(INFTY,'\n');
			infile >> dump;
		}
		
		infile >> x;
		infile >> y;

		
		infile.ignore(INFTY,'\n');
		
		assert(x != y);
		
		G.add_undirected(x-1,y-1);		
	}
	
	if(complement) G.flip();
	
}


void read_mtx(const std::string filename, CompactGraph& G) {
	std::ifstream infile(filename.c_str(), ios_base::in);
		
	string dump;
	
	infile >> dump;
		
	while(dump[0]=='\%') {
		infile.ignore(INFTY,'\n');
		infile >> dump;
	}
	
	int N, M;
	
	infile >> N;
	infile >> M;
		
	G.initialise(N);
	
	int x, y;
	for(int i=0; i<M; ++i) {

		infile >> x;
		infile >> y;		
		infile.ignore(INFTY,'\n');
		
		//std::cout << x << " " << y << std::endl;
		
		//assert(x != y);
			
		if(x!=y)	
			G.add_undirected(x-1,y-1);		
	}
}

void create_random(std::vector< std::vector<int> >& partition, const int N) {
	std::vector<int> vertices;
	for(int i=0; i<N; ++i) {
		vertices.push_back(i);
	}
	for(int i=0; i<N; ++i) {
		int r = i+randint(N-i);
		int x = vertices[i];
		vertices[i] = vertices[r];
		vertices[r] = x;
		
		//std::cout << " " << vetices[i]; 
		
		partition[i%(partition.size())].push_back(vertices[i]);
	}
	// for(int i=0; i<partition.size(); ++i) {
	// 	for(int j=0; j<partition[i].size(); ++j) {
	// 		std::cout << " " << partition[i][j];
	// 	}
	// 	std::cout << std::endl;
	// }
}


Variable cp_model(VarArray& X, Solver& solver, CompactGraph& G, std::vector< std::vector<int> >& partition, MISCmdLine& cmd) {
//const int vcoption, const int balance, const int filtering) {
	
	int balance = cmd.balance();
	
	int guide = 0;
	if (!strcmp(cmd.get_guide().c_str(),"witness")) guide = 1;
	if (cmd.get_witness() == 0 || cmd.get_guide() == "none") {
		if (cmd.get_witness() == 0) std::cout << " " << solver.parameters.prefix_comment << " No witness. Ignoring guiding policy and defaulting to \"none\"" << std::endl;
		guide = 0;
	}
	
	int weight = 0;
	if (!strcmp(cmd.get_weight().c_str(),"inwitness")) weight = 1;
	if (!strcmp(cmd.get_weight().c_str(),"outwitness")) weight = 2;
	if (!strcmp(cmd.get_weight().c_str(),"expl")) weight = 0;
	if (!strcmp(cmd.get_weight().c_str(),"none")) weight = -1;
	if (cmd.get_witness() == 0 && (weight != -1)) {
		std::cout << " " << solver.parameters.prefix_comment << " No witness. Ignoring weight policy and defaulting to \"none\"" << std::endl;
		weight = -1;
	}
	
	int branching = 0;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"inwitness")) branching = 1;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"outwitness")) branching = 2;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"mindegree")) branching = 3;
	if (!strcmp(cmd.get_branching_restrictions().c_str(),"maxdegree")) branching = 4;
	if (((cmd.get_witness() == 0) && (branching == 1 || branching == 2)) || cmd.get_branching_restrictions() == "all") {
		if (cmd.get_witness() == 0) std::cout << " " << solver.parameters.prefix_comment << " Ignoring branching restrictions and defaulting to \"all\"" << std::endl;
		branching = 0;
	}
	
	int lbrules = 0;
	if (!strcmp(cmd.get_lb_rules().c_str(),"CK")) lbrules = 1;
	if (!strcmp(cmd.get_lb_rules().c_str(),"NT")) lbrules = 2;
	if (!strcmp(cmd.get_lb_rules().c_str(),"NTCK")) lbrules = 3;
	
	bool add_maxdeg = !strcmp(cmd.get_vcheuristic().c_str(), "add_maxdegree");
	
	Variable K(0, G.size());

	solver.add(VertexCover(X,G,cmd.get_witness(),cmd.get_filtering(),cmd.get_backjump(),cmd.get_dominance(),weight,guide,branching,add_maxdeg,lbrules,cmd.get_decomp()) == K);
	
	if(partition.size()>1) {
		VarArray Cardinality;
		for(int i=0; i<partition.size(); ++i) {
		
			//std::cout << partition[i].size() << std::endl;
		
		
			Variable Oi(0,partition[i].size());

			VarArray pi;
			for(int j=0; j<partition[i].size(); ++j) {
				//std::cout << i << " " << j << ": " << partition[i][j] << std::endl;
			
				pi.add(X[partition[i][j]]);
			}

			solver.add(Sum(pi) == Oi);

			Cardinality.add(Oi);
		}

		if(balance>0) {
			solver.add((Max(Cardinality) - Min(Cardinality)) <= balance);
		} else {
			for(int i=1; i<Cardinality.size; ++i) {
				solver.add(Cardinality[i-1]==Cardinality[i]);
			}
		}
	}

	return K;
}


int main(int argc, char **argv)
{

	MISCmdLine cmd("VertexCover", ' ', "0.0");
	cmd.parse(argc, argv);

	if(cmd.print_parameters()) {
		std::cout << " c instance   = " << cmd.get_filename() << " #part=" << cmd.n_partition() << ", bal=" << cmd.balance() << std::endl;
		std::cout << " c problem    = " << cmd.get_problem_full() << std::endl;
		std::cout << " c format     = " << cmd.get_format() << std::endl;
		std::cout << " c vc branch  = " << cmd.get_vcheuristic() << std::endl;
		std::cout << " c seed       = " << cmd.get_seed() << std::endl;
		std::cout << " c weigh      = " << cmd.get_weight() << std::endl;
		std::cout << " c guiding    = " << cmd.get_guide() << std::endl;
		std::cout << " c branching  = " << cmd.get_branching_restrictions() << std::endl;
		std::cout << " c lb kernel  = " << cmd.get_lb_rules() << std::endl;
		std::cout << " c cutoff     = time: " ;
		if(cmd.get_tcutoff()>0) {
			std::cout << cmd.get_tcutoff() << " s";
		} else {
			std::cout << "none";
		}
		std::cout << " / search: ";
		if(cmd.get_ncutoff()>0) {
			std::cout << cmd.get_ncutoff() << " fails";
		} else {
			std::cout << "none";
		}
		std::cout << std::endl;
		std::cout << " c randomized = " << (cmd.get_randomization()>0 ? "yes" : "no") << std::endl;
		std::cout << " c restarts   = " << cmd.get_restart() << std::endl;
		std::cout << " c backjumps  = " << (cmd.get_backjump() ? "yes" : "no") << std::endl;
		std::cout << " c dominance  = " << (cmd.get_dominance() ? "yes" : "no") << std::endl;
		std::cout << " c filtering  = ";
		if (cmd.get_filtering() & 1) {
			std::cout << "rigid crowns";
			if ((cmd.get_filtering() >> 1) & 1) std::cout << " + Emmanuel's rule";
			if ((cmd.get_filtering() >> 2) & 1) std::cout << " + high degree pruning";
		} else if ((cmd.get_filtering() >> 1) & 1) {
			std::cout << "Emmanuel's rule";
			if ((cmd.get_filtering() >> 2) & 1) std::cout << " + high degree pruning";
		} else if ((cmd.get_filtering() >> 2) & 1) {
			std::cout << "high degree pruning";
		} else {
			std::cout << "nothing";
		}
		std::cout << std::endl;
		std::cout << " c strategy   = " << cmd.get_ordering() << " " << cmd.get_branching() << "(" << cmd.get_randomization() << ")" << std::endl;		
	}

	usrand(cmd.get_seed());

	CompactGraph G;


	if(cmd.get_filename()=="random") {
		int n = cmd.get_n();
		int m = cmd.get_m();
		G.initialise(n);
		G.initialise_random(m);
	} else {
		if(cmd.get_format() == "mtx") {
			read_mtx(cmd.get_filename(), G);
		} else if(cmd.get_format() == "mis") {
			read_dimacs(cmd.get_filename(), G, (cmd.get_problem()!="mis"));
		} else if(cmd.get_format() == "clq") {
			read_dimacs(cmd.get_filename(), G, (cmd.get_problem()!="mis"));
		} else if(cmd.get_format() == "snap") {
			read_snap(cmd.get_filename(), G);
		}
	}
	
	if(cmd.n_partition()==0) {
		ReversibleCompactGraph rG(G);
		VCAlgo<ReversibleCompactGraph> algo(rG, rG.size());		

		algo.fail_limit = cmd.get_ncutoff();
		algo.verbose = cmd.get_verbosity();
		algo.use_backjump = cmd.get_backjump();
		algo.use_cliques = cmd.get_dominance();
		algo.use_buss = true;
	
		algo.solve();
		algo.print_summary();
	} else{	


		std::vector< std::vector<int> > partition;
	
		int npart=cmd.n_partition();
		for(int i=0; i<npart; ++i) {
			vector<int> pi;
			partition.push_back(pi);
		}
	
		create_random(partition, G.size());
	
		Solver solver;
	
		VarArray X(G.size());

		Variable obj = cp_model(X, solver, G, partition, cmd); //.get_witness(), cmd.balance(), cmd.get_filtering());

		solver.minimize(obj);

		solver.consolidate();
	
		cmd.set_parameters(solver);
	
	
		solver.parameters.verbosity = cmd.get_verbosity();
	
		VarArray sorted;
	
		ReversibleCompactGraph rg(G);
	
		
		if(cmd.increasing()) {
			for(int d=rg.min_degree; d<=rg.max_degree; ++d) {
				if(!rg.node_of_degree[d].empty()) {
					for(int j=0; j<rg.node_of_degree[d].size; ++j) {
						sorted.add(X[rg.node_of_degree[d][j]]);
					}
				}
			}
		} else {
			for(int d=rg.max_degree; d>=rg.min_degree; --d) {
				if(!rg.node_of_degree[d].empty()) {
					for(int j=0; j<rg.node_of_degree[d].size; ++j) {
						sorted.add(X[rg.node_of_degree[d][j]]);
					}
				}
			}
		}
	
		BranchingHeuristic *branching_heuristic = solver.heuristic_factory(cmd.get_ordering(), cmd.get_branching(), cmd.get_randomization());
		RestartPolicy *restart_policy = solver.restart_factory(cmd.get_restart());
		
		
		
		if(cmd.print_model()) {
			std::cout << solver << std::endl;
		}
		
		solver.depth_first_search(sorted, branching_heuristic, restart_policy);
	
		//if(solver.solve() == OPT) {
		if(cmd.print_solution()) {
			if(solver.statistics.num_solutions) {
				std::cout << " " << solver.parameters.prefix_comment << " cover (" << obj.get_solution_int_value() << "): " ;
				for(int i=0; i<G.size(); ++i) {
					if(X[i].get_solution_int_value())
						std::cout << i << " ";
				}
				std::cout << std::endl;
			} else {
				std::cout << " " << solver.parameters.prefix_comment << " no solution" << std::endl;
			}
		}
	 
		 //std::cout << solver.statistics << std::endl;
		//std::cout << solver.statistics.print_vc_stats << std::endl;
   
		if(cmd.print_statistics())
			 solver.statistics.print_full(std::cout);
	}
	 
}



