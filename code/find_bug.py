#! /usr/bin/env python

import subprocess
import sys


s = 10
if len(sys.argv)>1:
    s = int(sys.argv[1])

r = 25
if len(sys.argv)>2:
    r = int(sys.argv[2])
    
rfile = open('tmp', 'w')

for n in range(s,s+r+1):
    for m in range(n, n*(n-1)/2):
        for b in range(4):
            ref = -1
            ref_cmd = None
            print n, m, b, 
            for f in range(4):
                for x in range(2):
                    cmdline = ['bin/balancedVC', '-f', 'random', '--npartition', '3', '--balance', str(b), '--filtering', str(f), '--witness', str(x), '-n', str(n), '-m', str(m)]
                    vcalgo = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    for line in vcalgo.stdout:
                        if line[:8] == ' c cover':
                            vc = int(line[line.find('(')+1:line.find(')')])
                            if ref == -1:
                                ref = vc
                                ref_cmd = cmdline
                            elif ref != vc:
                                print '\ndiscrepancy:'
                                print ' '.join(ref_cmd)
                                print ' '.join(cmdline)
                                sys.exit()
                        elif line[:4] == ' o |':
                            nn = line.split()[2]
                    rfile.write(nn+'\n')        
                    for line in vcalgo.stderr:
                        print
                        print ' '.join(cmdline)
                        sys.exit()
                    vcalgo.wait()
                    print '.',
            print
            
rfile.close()


    


