import subprocess


############ Things to change manually ###############

key = 'test'

timeout = 200
#range_part = [4]
#range_seed = [12345]
#range_bal = [1,3]
range_part = [4]
range_seed = [12345]
range_bal = [0,2,4]
benchpaths = ['../benchmark/DIMACS_MIS/','../benchmark/DIMACS_cliques/']

solvers = ['vanilla_lex','vanilla_lex_fil','witness_lex_high','witness_lex_highfil','witness_lex_mid','witness_lex_midfil','witness_lex_low']

cmdlines = {}.fromkeys(solvers)
cmdlines['vanilla'] = ['--filtering', '0', '--witness', '0']
cmdlines['full'] = ['--filtering', '3', '--witness', '100000']
cmdlines['nowitness'] = ['--filtering', '1', '--witness', '0']
cmdlines['nocrown'] = ['--filtering', '2', '--witness', '100000']
cmdlines['norule'] = ['--filtering', '1', '--witness', '100000']

cmdlines['vanilla_lex'] = ['--filtering', '0', '--witness', '0', '-c', 'lexicographic','--increasing']
cmdlines['vanilla_lex_fil'] = ['--filtering', '1', '--witness', '0', '-c', 'lexicographic','--increasing']
cmdlines['witness_lex_low'] = ['--filtering', '0', '--witness', '100', '-c', 'lexicographic','--increasing']
cmdlines['witness_lex_mid'] = ['--filtering', '0', '--witness', '5000', '-c', 'lexicographic','--increasing']
cmdlines['witness_lex_high'] = ['--filtering', '0', '--witness', '100000', '-c', 'lexicographic','--increasing']
cmdlines['witness_lex_midfil'] = ['--filtering', '1', '--witness', '5000', '-c', 'lexicographic','--increasing']
cmdlines['witness_lex_highfil'] = ['--filtering', '1', '--witness', '100000', '-c', 'lexicographic','--increasing']


cmdlines['vanilla_lex_dec'] = ['--filtering', '0', '--witness', '0', '-c', 'lexicographic']
cmdlines['vanilla_lex_inc'] = ['--filtering', '0', '--witness', '0', '-c', 'lexicographic', '--increasing']
cmdlines['vanilla_mindeg'] = ['--filtering', '0', '--witness', '0', '-c', 'dom/gwdeg', '--branch_on', 'mindegree']
cmdlines['vanilla_maxdeg'] = ['--filtering', '0', '--witness', '0', '-c', 'dom/gwdeg', '--branch_on', 'maxdegree']
cmdlines['vanilla_mindeg_fil'] = ['--filtering', '1', '--witness', '0', '-c', 'dom/gwdeg', '--branch_on', 'mindegree']
cmdlines['vanilla_maxdeg_fil'] = ['--filtering', '1', '--witness', '0', '-c', 'dom/gwdeg', '--branch_on', 'maxdegree']
cmdlines['vanilla_mindeg_witfil'] = ['--filtering', '1', '--witness', '500000', '-c', 'dom/gwdeg', '--branch_on', 'mindegree']
cmdlines['vanilla_maxdeg_witfil'] = ['--filtering', '1', '--witness', '500000', '-c', 'dom/gwdeg', '--branch_on', 'maxdegree']
######################################################



common_options = ['-t', str(timeout), '--print_sta', '-b', 'minval','-r','no']

def get_bench(benchpath):
    benchs = []
    ls = subprocess.Popen(['ls', '-1', benchpath], stdout=subprocess.PIPE)
    for line in ls.stdout:
        benchs.append(line[:-1])
    ls.wait()
    return benchs
    

def run_exp(benchs, doneruns):
    for benchpath in benchpaths:  
   	benchs = get_bench(benchpath)  
        for bench in benchs:
            for npart in range_part:
                for bal in range_bal:
                    for seed in range_seed:
                        for solver in solvers:
                            print 'solve', bench, npart, bal, 'with', solver,
            
                            run = bench+'.'+str(npart)+'.'+str(bal)+'.'+solver+'.'+str(seed)
                        
                            if not run in doneruns:
            
                                logfile = open(key+'.log', 'a+')
                                outfile = open('results/'+key+'.'+run+'.sol', 'w')
                                errfile = open('results/'+key+'.'+run+'.err', 'w')
                    
                                cmdline = ['../bin/balancedVC']
                                cmdline.extend(['-f', benchpath+bench])
                                cmdline.extend(['--npartition', str(npart), '--balance', str(bal), '--seed', str(seed)])
                                cmdline.extend(cmdlines[solver])
                                cmdline.extend(common_options)
                                if benchpath == '../benchmark/DIMACS_cliques/':
                                    cmdline.extend(['--problem', 'clq'])
                                else:
                                    cmdline.extend(['--problem', 'mis'])

                                print ' '.join(cmdline)

                                arun = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    
                                outfile.write(' '.join(cmdline)+'\n')
                    
                                for line in arun.stdout:
                                    outfile.write(line)
                                for line in arun.stderr: 
                                    print 'error:', line,
                                    errfile.write(line)
                                arun.wait()
                                outfile.close()
                                errfile.close()
                                logfile.write(run+'\n')
                                logfile.close()
                                print 'run finished\n'
                            else:
                                print 'already done'

nbench = 0
for benchpath in benchpaths: 
    benchs = get_bench(benchpath)
    nbench = nbench + len(benchs);
nb_runs = len(range_part)*len(range_bal)*len(range_seed)*nbench


logfile = open(key+'.log', 'a+')
doneruns = set([line[:-1] for line in logfile])
logfile.close()


print nb_runs, '(-'+str(len(doneruns))+')', 'runs in at most', (nb_runs-len(doneruns))*timeout, 'seconds'


run_exp(benchs, doneruns)
