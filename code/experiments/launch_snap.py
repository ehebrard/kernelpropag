import subprocess
import math

############ Things to change manually ###############

timeout = 100
benchpath = '../benchmark/snap/'

solvers = ['vanilla','vanilla_NTCK','nowitness_NTCK','norule','norule_NTCK']
#solvers = ['nowitness']

ratio = 0.001

cmdlines = {}.fromkeys(solvers)
cmdlines['vanilla'] = ['--filtering', '0', '--witness', '0']
cmdlines['vanilla_NTCK'] = ['--filtering', '0', '--witness', '0','--lbkernel','NTCK']
cmdlines['full'] = ['--filtering', '3', '--witness', '1000']
cmdlines['nowitness'] = ['--filtering', '1', '--witness', '0']
cmdlines['nowitness_NTCK'] = ['--filtering', '1', '--witness', '0','--lbkernel','NTCK']
cmdlines['nocrown'] = ['--filtering', '2', '--witness', '1000']
cmdlines['norule'] = ['--filtering', '1', '--witness', '1000']
cmdlines['norule_NTCK'] = ['--filtering', '1', '--witness', '1000','--lbkernel','NT']
cmdlines['extrafull'] = ['--filtering', '7', '--witness', '1000']
cmdlines['extrafull_nowitness'] = ['--filtering', '7', '--witness', '0']
cmdlines['witness'] = ['--filtering', '0', '--witness', '1000']



######################################################

common_options = ['-t', str(timeout), '--format', 'snap', '--print_sta', '-c', 'lexicographic', '-b', 'minval', '--npartition', '3','-r','no']

def get_bench():
    benchs = []
    ls = subprocess.Popen(['ls', '-1', benchpath], stdout=subprocess.PIPE)
    for line in ls.stdout:
        benchs.append(line[:-1])
    ls.wait()
    return benchs

def run_exp(benchs):
	resultfile = open('results_snap/summary.txt','w')
	for bench in benchs:
	
		if bench[-1] == '~':
			continue
	
		resultfile.write('bench : '+bench+'\n')
		
		# read the number of nodes
		infile = open(benchpath+bench)
		for line in infile:
			data = line[:-1].split()
			if data[0] == 'p':
				n = float(data[2])
				break
		infile.close()
		
		for solver in solvers:
			resultfile.write('solver '+solver+': ')
			outfile = open('results_snap/'+bench+solver+'.sol', 'w')
			errfile = open('results_snap/'+bench+solver+'.err', 'w')
			
			cmdline = ['../bin/balancedVC']
			cmdline.extend(['-f', benchpath+bench])
			cmdline.extend(['--balance', str(int(math.floor(n*ratio)))])
			cmdline.extend(cmdlines[solver])
			cmdline.extend(common_options)
				
			print ' '.join(cmdline)
			arun = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			
			outfile.write(' '.join(cmdline)+'\n')
			for line in arun.stdout:
				outfile.write(line)
			for line in arun.stderr:
				print 'error:', line,
				errfile.write(line)
			arun.wait()
			outfile.close()
			errfile.close()
			
			outfile = open('results_snap/'+bench+solver+'.sol', 'r')
			for line in outfile:
				data = line[:-1].split()
				if data[0] == 's':
					resultfile.write(str(data[1])+' | ')
				if data[0] == 'd':
                    			if data[1] == 'OBJECTIVE':
                    				resultfile.write('OBJ '+str(data[2])+' | ')
                    			if data[1] == 'NODES':
                    				resultfile.write('NODES '+str(data[2])+' | ')
                    			if data[1] == 'RUNTIME':
                    				resultfile.write('TIME '+str(data[2])+' | ')
                    	resultfile.write('\n')
                 	outfile.close()
	resultfile.close()
	
benchs = get_bench()
run_exp(benchs)
