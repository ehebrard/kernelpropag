import subprocess


############ Things to change manually ###############

timeout = 1000
benchpath = '../benchmark/MCS/r005/'
benchprefix = 'mcs90_r005_s'
##benchpath = '../benchmark/MCS/m4Dr6/'
##benchprefix = 'mcs50_m4Dr6_s'
##benchpath = '../benchmark/MCS/b06/'
##benchprefix = 'mcs90_b06_s'

range_size = [20,25,30]
range_nb = range(100)

##solvers = ['vanilla', 'nowitness', 'full', 'nocrown', 'norule', 'extrafull', 'extrafull_nowitness']
##solvers = ['vanilla_lexico','witness_lexico','vanilla_lexico_CK','vanilla_maxdegree']
solvers = ['vanilla_lexico']

cmdlines = {}.fromkeys(solvers)
cmdlines['vanilla'] = ['--filtering', '0', '--witness', '0']
cmdlines['vanilla_lexico'] = ['--filtering', '0', '--witness', '0', '--basic-model', '-c', 'lexicographic']
cmdlines['witness_lexico'] = ['--filtering', '0', '--witness', '10', '--basic-model', '-c', 'lexicographic']
cmdlines['witness_lexico_CK'] = ['--filtering', '0', '--witness', '10', '--basic-model', '-c', 'lexicographic','--lbkernel','CK']
cmdlines['witness_lexico_NT'] = ['--filtering', '0', '--witness', '10', '--basic-model', '-c', 'lexicographic','--lbkernel','NT']
cmdlines['vanilla_lexico_CK'] = ['--filtering', '0', '--witness', '0', '--basic-model', '-c', 'lexicographic','--lbkernel','CK']
cmdlines['vanilla_lexico_NT'] = ['--filtering', '0', '--witness', '0', '--basic-model', '-c', 'lexicographic','--lbkernel','NT']
cmdlines['vanilla_lexico_NTCK'] = ['--filtering', '0', '--witness', '0', '--basic-model', '-c', 'lexicographic','--lbkernel','NTCK']
cmdlines['vanilla_mindegree'] = ['--filtering', '0', '--witness', '0', '--basic-model', '--branch_on', 'mindegree', '-c', 'dom/gwdeg']
cmdlines['vanilla_maxdegree'] = ['--filtering', '0', '--witness', '0', '--basic-model', '--branch_on', 'maxdegree', '-c', 'dom/gwdeg']
cmdlines['witness_outwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'outwitness', '--branch_on', 'outwitness']


cmdlines['full'] = ['--filtering', '3', '--witness', '10']
cmdlines['nowitness'] = ['--filtering', '1', '--witness', '0']
cmdlines['nocrown'] = ['--filtering', '2', '--witness', '10']
cmdlines['norule'] = ['--filtering', '1', '--witness', '10']
cmdlines['witness'] = ['--filtering', '0', '--witness', '10']
cmdlines['extrafull'] = ['--filtering', '7', '--witness', '10']
cmdlines['extrafull_nowitness'] = ['--filtering', '7', '--witness', '0']

cmdlines['wnone-inwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'none', '--branch_on', 'inwitness']
cmdlines['wnone-outwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'none', '--branch_on', 'outwitness']
cmdlines['wnone-all'] = ['--witness', '10', '--filtering', '0', '--weight', 'none', '--branch_on', 'all']
cmdlines['wexpl-inwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'expl', '--branch_on', 'inwitness']
cmdlines['wexpl-outwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'expl', '--branch_on', 'outwitness']
cmdlines['wexpl-all'] = ['--witness', '10', '--filtering', '0', '--weight', 'expl', '--branch_on', 'all']
cmdlines['winwitness-inwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'inwitness', '--branch_on', 'inwitness']
cmdlines['winwitness-outwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'inwitness', '--branch_on', 'outwitness']
cmdlines['winwitness-all'] = ['--witness', '10', '--filtering', '0', '--weight', 'inwitness', '--branch_on', 'all']
cmdlines['woutwitness-inwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'outwitness', '--branch_on', 'inwitness']
cmdlines['woutwitness-outwitness'] = ['--witness', '10', '--filtering', '0', '--weight', 'outwitness', '--branch_on', 'outwitness']
cmdlines['woutwitness-all'] = ['--witness', '10', '--filtering', '0', '--weight', 'outwitness', '--branch_on', 'all']

cmdlines['winwitnessgeom-all'] = ['--witness', '10', '--filtering', '0', '--weight', 'inwitness_geom', '--branch_on', 'all']
cmdlines['wnone-all-plus'] = ['--witness', '100', '--filtering', '0', '--weight', 'none', '--branch_on', 'all']
cmdlines['woutwitness-all-plus'] = ['--witness', '100', '--filtering', '0', '--weight', 'outwitness', '--branch_on', 'all']
######################################################



##common_options = ['-t', str(timeout), '--print_sta', '-c', 'dom/gwdeg', '-b', 'minval', '--connected', '--ignore-labels', '--backjump']
##common_options = ['-t', str(timeout), '--print_sta', '-c', 'dom/gwdeg', '-b', 'minval', '--connected', '--ignore-labels', '--backjump', '--increasing', '--weight', 'none', '--branch_on', 'outwitness']
common_options = ['-t', str(timeout), '--print_sta', '-b', 'minval', '--connected', '--ignore-labels', '--vcheuristic','rem_mindegree','--weightc','both','-r','no']
def run_exp():
	resultfile = open('results_subiso/summary.txt','w')
	for solver in solvers:
		resultfile.write('=============== SOLVER '+solver+' ===============\n')
		for size in range_size:
			totaltime = 0.0
			totalnodes = 0.0
			totalobj = 0.0
			for nb in range_nb:
				if nb < 10:
					afile = benchprefix+str(size)+'.A0'+str(nb)
					bfile = benchprefix+str(size)+'.B0'+str(nb)
				else:
					afile = benchprefix+str(size)+'.A'+str(nb)
					bfile = benchprefix+str(size)+'.B'+str(nb)
				outfile = open('results_subiso/'+benchprefix+solver+str(size)+'_'+str(nb)+'.sol', 'w')
				errfile = open('results_subiso/'+benchprefix+solver+str(size)+'_'+str(nb)+'.err', 'w')
			
				cmdline = ['../bin/subiso']
				cmdline.extend(['-f', benchpath+afile])
				cmdline.extend(['--fileb', benchpath+bfile])
				cmdline.extend(cmdlines[solver])
				cmdline.extend(common_options)
				
				outfile.write(' '.join(cmdline)+'\n')
				
				arun = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		
				for line in arun.stdout:
					outfile.write(line)
				for line in arun.stderr:
					print 'error:', line,
					errfile.write(line)
				arun.wait()
				outfile.close()
				errfile.close()
			
				outfile = open('results_subiso/'+benchprefix+solver+str(size)+'_'+str(nb)+'.sol', 'r')
				for line in outfile:
					data = line[:-1].split()
					if data[0] == 'd':
                    				if data[1] == 'RUNTIME':
                    					print solver + '# size '+str(size)+', nb '+str(nb)+ ': '+str(data[2])
                    					totaltime = totaltime + float(data[2])
                    				if data[1] == 'NODES':
                    					totalnodes = totalnodes + float(data[2])
                    				if data[1] == 'OBJECTIVE':
                    					totalobj = totalobj + float(data[2])
                 		outfile.close()
			avgtime = totaltime/len(range_nb)
			avgnodes = totalnodes/len(range_nb)
			avgobj = totalobj/len(range_nb)
			resultfile.write('** SIZE '+str(size)+'\n')
			resultfile.write('***** Avg time '+str(avgtime)+'\n')
			resultfile.write('***** Avg nodes '+str(avgnodes)+'\n')
			resultfile.write('***** Avg objective '+str(avgobj)+'\n')
	resultfile.close()
run_exp()
