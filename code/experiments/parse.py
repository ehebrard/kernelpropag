import subprocess
import math
import sys

#key = 'test'

solver_rank = ['decomp', 'vanilla_lex_rien', 'vanilla_lex', 'witness_lex_midfil', 'witness_lex_midfilplus', 'witness_lex_midfilplusNT', 'snap_decomp_lex', 'snap_vanilla_lex_rien', 'snap_crowns_lex', 'snap_wit_lex', 'snap_witplus_lex', 'snap_witplusNT_lex']
default_keys = ['decomp.log', 'crowns.log', 'vanilla.log', 'vanilla_rien.log', 'witness.log', 'witplus.log', 'snap_decomp.log', 'snap_crowns.log', 'snap_vanilla.log', 'snap_vanilla_rien.log', 'snap_wit.log', 'snap_witplus.log']
with_snap = True


print ' '.join(default_keys)

if len(sys.argv) < 2:
    print 'usage: python parse.py <key1> ... <keyn> (using default)'
else:
    default_keys = sys.argv[1:]
    with_snap = False
    for x in default_keys:
      if x.find('snap') >= 0:
        with_snap = True
        break
    
    
    # sys.exit(1)
    
    
mapping = {}
    
mapping['snap_decomp_lex'] = 'decomp'
mapping['snap_vanilla_lex_rien'] = 'vanilla_lex_rien'
mapping['snap_vanilla_lex'] = 'vanilla_lex'
mapping['snap_crowns_lex'] = 'vanilla_lex_fil'
mapping['snap_wit_lex'] = 'witness_lex_midfil'
mapping['snap_witplus_lex'] = 'witness_lex_midfilplus'
mapping['snap_witplusNT_lex'] = 'witness_lex_midfilplusNT'
mapping['decomp'] = 'decomp'
mapping['vanilla_lex_rien'] = 'vanilla_lex_rien'
mapping['vanilla_lex'] = 'vanilla_lex'
mapping['vanilla_lex_fil'] = 'vanilla_lex_fil'
mapping['witness_lex_midfil'] = 'witness_lex_midfil'
mapping['witness_lex_midfilplus'] = 'witness_lex_midfilplus'
mapping['witness_lex_midfilplusNT'] = 'witness_lex_midfilplusNT'


bmapping = {}
bmapping['0'] = "tight"
bmapping['2'] = "betweentightandmedium"
bmapping['4'] = "medium"
bmapping['8'] = "loose"
bmapping['16'] = "extraloose"
bmapping['0.003'] = "gigatight"
bmapping['0.005'] = "tighterer"
bmapping['0.006'] = "tighter"
bmapping['0.007'] = "tight"
bmapping['0.008'] = "medium"
bmapping['0.009'] = "loose"
bmapping['0.01'] = "extraloose"


# solver_rank = dict([('decomp',1), ('vanilla_lex_rien':3), ('vanilla_lex_fil',5), ('witness_lex_midfil',7), ('witness_lex_midfilplus',9), ('witness_lex_midfilplusNT',11), ('snap_decomp_lex',2), ('snap_vanilla_lex_rien',4), ('snap_crowns_lex',6), ('snap_wit_lex',8), ('snap_witplus_lex',10), ('snap_witplusNT_lex',12)]
solver_rank = ['decomp', 'vanilla_lex_rien', 'vanilla_lex_fil', 'witness_lex_midfil', 'witness_lex_midfilplus', 'witness_lex_midfilplusNT', 'snap_decomp_lex', 'snap_vanilla_lex_rien', 'snap_crowns_lex', 'snap_wit_lex', 'snap_witplus_lex', 'snap_witplusNT_lex']

extensions = set(['clq','mis','txt','col','vc'])

class Index(list):
    def __init__(self, filenames):

        self.benches   = set([])
        self.solvers  = set([])
        self.seeds    = set([])
        self.nparts   = set([])
        self.balances = set([])
        
        self.pretty = {}
        self.pretty['decomp'] = '\\decomp'
        self.pretty['vanilla_lex_rien'] = '\\onlylb'
        self.pretty['vanilla_lex'] = '\\clique'
        self.pretty['vanilla_lex_fil'] = '\\kernelp'
        self.pretty['witness_lex_midfil'] = '\\witness'
        self.pretty['witness_lex_midfilplus'] = '\\all'
        self.pretty['witness_lex_midfilplusNT'] = '\\allp'

        self.pretty['snap_decomp_lex'] = '\\decomp'
        self.pretty['snap_vanilla_lex_rien'] = '\\onlylb'
        self.pretty['snap_vanilla_lex'] = '\\clique'
        self.pretty['snap_crowns_lex'] = '\\kernelp'
        self.pretty['snap_wit_lex'] = '\\witness'
        self.pretty['snap_witplus_lex'] = '\\all'
        self.pretty['snap_witplusNT_lex'] = '\\allp'
        

        for filename in filenames:
            
            key = (filename.split('.'))[0]
            keyfile = open(filename, 'r')

            for line in keyfile:
                run = line[:-1]
    
                drun = {}
                
                # drun['key'] = key
                drun['cmd'] = key+'.'+run
    
                data = run.split('.')

         
                if len(data) == 6:
                    drun['bench'] = data[0]
                    drun['npart'] = int(data[2])
                    drun['balance'] = bmapping[data[3]]
                    drun['solver'] = mapping[data[4]]
                    drun['seed'] = int(data[5])
                    if data[1] not in extensions:
                        print 'unknown extension', data[1]
                        sys.exit(1)
                
                elif len(data) == 7:
                    if data[1] == 'txt':
                        drun['bench'] = data[0]
                        drun['npart'] = int(data[2])
                        drun['balance'] = bmapping[(data[3]+'.'+data[4])]
                        drun['solver'] = mapping[data[5]]
                        drun['seed'] = int(data[6])
                        if data[1] not in extensions:
                            print 'unknown extension', data[1]
                            sys.exit(1)                
                    else:
                        drun['bench'] = (data[0]+'.'+data[1])
                        drun['npart'] = int(data[3])
                        drun['balance'] = bmapping[data[4]]
                        drun['solver'] = mapping[data[5]]
                        drun['seed'] = int(data[6])
                        if data[2] not in extensions:
                            print 'unknown extension', data[2]
                            sys.exit(1)
                
                elif len(data) == 8:
                    drun['bench'] = (data[0]+'.'+data[1]+'.'+data[2])
                    drun['npart'] = int(data[4])
                    drun['balance'] = bmapping[data[5]]
                    drun['solver'] = mapping[data[6]]
                    drun['seed'] = int(data[7])
                    if data[3] not in extensions:
                        print 'unknown extension', data[3]
                        sys.exit(1)                
                        
                if len(data)>=6 :
                    self.benches.add(drun['bench'])
                    self.nparts.add(drun['npart'])
                    self.balances.add(drun['balance'])
                    self.solvers.add(drun['solver'])
                    self.seeds.add(drun['seed'])
                    
                    self.append(drun)        
                    
            keyfile.close()
    
        self.sorted_benches = list(self.benches)
        self.sorted_benches.sort()
        self.sorted_nparts = list(self.nparts)
        self.sorted_nparts.sort()
        self.sorted_balances = list(self.balances)
        self.sorted_balances.sort()
        # self.sorted_solvers = list(self.solvers)
        # self.sorted_solvers.sort()
        self.sorted_solvers = [slv for slv in solver_rank if slv in self.solvers]
        
        # print self.sorted_solvers
        
        self.sorted_seeds = list(self.seeds)
        self.sorted_seeds.sort()
        
        self.classes = {}
        last = ''
        for bench in self.sorted_benches:
            if len(self.classes)==0 or last != bench[:3]:
                last = bench[:3]
                self.classes[last] = [bench]
            else:
                self.classes[last].append(bench)
                
                
                
    def compute_ratios(self, data):
        self.vcratio = {}.fromkeys(self.sorted_benches)
        for bench in self.sorted_benches:
            # maxbal = max(self.balances)
            minpart = min(self.nparts) 
            aseed = self.sorted_seeds[0]
            minvc = min([data[solver][bench][bal][minpart][aseed]['OBJECTIVE'] for solver in self.sorted_solvers for bal in ['loose','extraloose'] if data[solver][bench][bal][minpart][aseed] != None])
            
            # print self.sorted_solvers[0], bench
            # print data[self.sorted_solvers[0]][bench]
            
            gsize = 0
            for solver in self.sorted_solvers:
                if data[solver][bench]['loose'][minpart][aseed] != None:
                    gsize = data[solver][bench]['loose'][minpart][aseed]['VARIABLES']
            # data['witness_lex_midfilplusNT'][bench]['medium'][minpart][aseed]['VARIABLES']
            self.vcratio[bench] = float(minvc)/float(gsize)
            # print bench, self.vcratio[bench], minvc, gsize
            
        for bclass in self.classes.keys():
            total = 0.0
            for bench in self.classes[bclass]:
                total += self.vcratio[bench]
            self.vcratio[bclass] = total/float(len(self.classes[bclass]))
            # print bclass, self.vcratio[bclass]
            
        self.sorted_classes = [c for r,c in sorted([(self.vcratio[bclass], bclass) for bclass in self.classes.keys() if bclass != 'CA-'], reverse=True)]
        if with_snap:
          self.sorted_classes.append('CA-')
          
        self.sorted_benches_ratio = [b for r,b in sorted([(min([data[solver][b][bal][min(self.nparts)][self.sorted_seeds[0]]['OBJECTIVE'] for solver in self.sorted_solvers for bal in ['loose','extraloose'] if data[solver][b][bal][min(self.nparts)][self.sorted_seeds[0]] != None]), b) for b in self.sorted_benches], reverse=True)]
        
        
    def head(self):
        ret_str = '\\begin{scriptsize}\n\\begin{tabular}{ll'+''.join(['|rrrr']*len(self.solvers))+'}\n\multicolumn{2}{c}{}'
        #\multirow{2}{*}{bench}}'
        for x in self.sorted_solvers:
            ret_str += ' & \multicolumn{4}{c}{'
            ret_str += self.pretty[x]
            ret_str += '}'
        ret_str += '\\\\\n & '
        for x in self.sorted_solvers:
            ret_str += ' & \#s & gap & cpu & \#nd'
        ret_str += '\\\\\n\\hline\n'
        return ret_str
        
    def foot(self):
        return '\\end{tabular}\n\\end{scriptsize}\n'	
        

class DataPoint(dict):
    def __init__(self,run):
        resfile = open('results/'+run+'.sol', 'r')
        
        self.curve = []
        
        for line in resfile:
            data = line[:-1].split()
            if len(data) == 2:
                if data[0] == 's':
                    if data[1] == 'LIMITOUT':
                        self['SOLVED'] = 0
                    else:
                        self['SOLVED'] = 1
                    if data[1] == 'OPTIMAL':
                        self['OPTIMAL'] = 1
                    else:
                        self['OPTIMAL'] = 0
            elif len(data) == 3:
                if data[0] == 'd':
                    self[data[1]] = float(data[2])
            elif len(data) == 8:
                if data[0] == 'o':
                    self.curve.append((int(data[6]), int(data[2]), int(data[3]), float(data[4])))
        
        resfile.close()

def compute_best(index, data, stats):
    data['best'] = {}.fromkeys(index.benches)
    for bench in index.benches:
        data['best'][bench] = {}.fromkeys(index.balances)
        for npart in index.nparts:
            data['best'][bench][npart] = {}.fromkeys(index.seeds)
            for seed in index.seeds:
                data['best'][bench][npart][seed] = {}.fromkeys(stats)
                for stat in stats:
                    bestval = min([data[solver][bench][balance][npart][seed][stat] for solver in index.solvers for balance in index.balances if data[solver][bench][balance][npart][seed] != None])
                    data['best'][bench][npart][seed][stat] = bestval
 
 
def read_data(index):
    data = {}.fromkeys(index.solvers)
    for solver in index.solvers:
        data[solver] = {}.fromkeys(index.benches)
        for bench in index.benches:
            data[solver][bench] = {}.fromkeys(index.balances)
            for balance in index.balances:
                data[solver][bench][balance] = {}.fromkeys(index.nparts)
                for npart in index.nparts:
                    data[solver][bench][balance][npart] = {}.fromkeys(index.seeds)
                    for seed in index.seeds:
                        data[solver][bench][balance][npart][seed] = None
    for run in index:
        
        data[run['solver']][run['bench']][run['balance']][run['npart']][run['seed']] = DataPoint(run['cmd'])
            
    compute_best(index, data, ['OBJECTIVE']) #, 'RUNTIME', 'BESTRUNTIME', 'BESTNODES'])


    return data
    
    
       
        
    
    
class Table(dict):
    def __init__(self,X,Y):
        
        self.X = X
        self.Y = Y
    
    
        self.max_width = {}.fromkeys(X)
        for x in X:
            self[x] = {}.fromkeys(Y)
            self.max_width[x] = len(x)
            for y in Y:
                self[x][y] = -1
        
        self.max_width_caption = 0
        for y in Y:
            if len(y) > self.max_width_caption:
                self.max_width_caption = len(y)
                
    
    # def __str__(self):
    #     w = 2 + sum([self.max_width[x] for x in self.X]) + self.max_width_caption + len(self.X)
    #     return ' '.ljust(self.max_width_caption) + ' | ' + ' '.join([str(x).center(self.max_width[x]) for x in self.X]) + '\n' + ('=' * w) + '\n' + '\n'.join([str(y).rjust(self.max_width_caption) + ' | ' + ' '.join([' '.join([('%.2f'%(v)) for v in self[x][y]]).rjust(self.max_width[x])] for x in self.X]) for y in self.Y])
    #
    
    
    def get_magnitude(self, x):
        if x<1000:
            return int(x), ''
        elif x<100000:
            return int(float(x/1000.0)), 'K'
        else:
            return float(x)/1000000.0, 'M'
         
        
    
    def latex(self, index, bal, classes):
        # slv = [x for x in self.X]
        # slv.append(slv[-1])
        # classes = index.sorted_classes
        slv = self.X
        
        # print slv
        
        # classes = index.sorted_classes
        
        byclasses = (len(classes) == len(index.sorted_classes))

        
        # if byclasses:
        ret_str = '&'      
        ret_str += ' \multicolumn{%i}{c}{balancing constraint: %s}\\\\'%(1+4*len(slv), bal)
        for y in classes:
            
            # if y == 'p2p':
            #     ret_str += '\\hline\n'
                
                            
            themin = min([self[x][y] for x in self.X])
            
            if byclasses :
                ret_str += str(len(index.classes[y]))
                ret_str += ' & \\texttt{%s}'%(y.replace('_','\_').lower())
            else:
                ret_str += ' \multicolumn{2}{c}{%s}'%y.replace('_','\_').lower()
            
            for x in slv:
                is_best = True
                
                # print themin[:4], '<', self[x][y][:4]
                # print themin[4], '<', (self[x][y][4]+1)
                # print themin[5], '<' (self[x][y][5]*1.01)
                
                # print themin
                
                if themin[:3] < self[x][y][:3] :
                    is_best = False
                elif themin[3] != None:
                    if self[x][y][3] == None:
                        is_best = False
                    elif (themin[3]+1) < self[x][y][3] :
                        is_best = False
                    elif (themin[4]*1.01) < self[x][y][4] :
                        is_best = False
                
                # print x, y, self[x][y]
                
                s = int(self[x][y][0]+self[x][y][1])
                o = '-'
                if self[x][y][2] != None:
                    if byclasses:
                        o = '%.2f'%self[x][y][2]
                    else:
                        o = '%i'%self[x][y][2]
                t = '-'
                if self[x][y][3] != None:
                    t = '%.1f'%self[x][y][3]
                n = '-'
                if self[x][y][4] != None:
                    value, magnitude = self.get_magnitude(self[x][y][4])
                    if magnitude != 'M':
                        n = '%i%s'%(value, magnitude)
                    else:
                        n = '%.1f%s'%(value, magnitude)
                        
                if is_best:
                    ret_str += ((' & \cellcolor{TealBlue!30}{%s} & \cellcolor{TealBlue!30}{%s} & \cellcolor{TealBlue!30}{%s} & \cellcolor{TealBlue!30}{%s}')%(s,o,t,n))
                else:
                    ret_str += ((' & %s & %s & %s & %s')%(s,o,t,n))
                
                # if magnitude != 'M':
                #     if is_best:
                #         ret_str += ((' & \cellcolor{TealBlue!40}{%i} & \cellcolor{TealBlue!40}{%.2f} & \cellcolor{TealBlue!40}{%.1f} & \cellcolor{TealBlue!40}{%i}'+magnitude)%(self[x][y][0]+self[x][y][1], self[x][y][2], self[x][y][3], value))
                #     else:
                #         ret_str += ((' & %i & %.2f & %.1f & %i'+magnitude)%(self[x][y][0]+self[x][y][1], self[x][y][2], self[x][y][3], value))
                # else:
                #     if is_best:
                #         ret_str += ((' & \cellcolor{TealBlue!40}{%i} & \cellcolor{TealBlue!40}{%.2f} & \cellcolor{TealBlue!40}{%.1f} & \cellcolor{TealBlue!40}{%.1f}'+magnitude)%(self[x][y][0]+self[x][y][1], self[x][y][2], self[x][y][3], value))
                #     else:
                #
                #         ret_str += ((' & %i & %.2f & %.1f & %.1f'+magnitude)%(self[x][y][0]+self[x][y][1], self[x][y][2], self[x][y][3], value))
            ret_str += '\\\\\n'
        
        return ret_str
        
        
    
    def __iadd__(self, tpl):
        x = tpl[0]
        y = tpl[1]
        val = tpl[2]
        sval = str(val)
        if self.max_width[x] < len(sval):
            self.max_width[x] = len(sval)
        self[x][y] = val
        
        return self
        
        
        
def compute_avg(index,data,stat):
    
    # print 'compute average', stat, 'of', data
    
    value = 0.0
    count = 0
    #for balance in index.balances:
    for npart in index.nparts:
        for seed in index.seeds:
            if data[npart][seed] != None:
                # print 'data['+str(npart)+']['+str(seed)+']['+stat+'] =', data[npart][seed][stat]
                value += data[npart][seed][stat]
                count += 1
    if count>0:
        return value/count
    return None
    
    
# def compute_geo_avg(index,data,stat):
#     value = 0.0
#     count = 0
#     for npart in index.nparts:
#         for seed in index.seeds:
#             if data[npart][seed] != None:
#                 value += data[npart][seed][stat]
#                 count += 1
#     if count>0:
#         return value/count
#     return None
    
    
    
def compute_min(index,data,stat):
    value = None
    #for balance in index.balances:
    for npart in index.nparts:
        for seed in index.seeds:
            if data[npart][seed] != None:
                if value == None:
                    value = data[npart][seed][stat]
                elif value > data[npart][seed][stat]:
                    value = data[npart][seed][stat]
    return value
        
    
    
def get_mean(index, data, solver, bal, stat, benches, geom):

    # print benches

    value = 0.0
    count = 0
    for bench in benches:
        for npart in index.nparts:
            for seed in index.seeds:
                
                # print solver, bench, bal, stat
                # if stat == 'OBJECTIVE':
                #   print data['best'][bench][npart][seed]
                # print data[solver][bench][bal][npart][seed]
                # print
                
                if data[solver][bench][bal][npart][seed] != None:
                    if stat=='OBJECTIVE':
                        count += 1
                        aval = (data[solver][bench][bal][npart][seed][stat] - data['best'][bench][npart][seed][stat])
                        if geom:
                            if aval == 0:
                                aval = 0.000001
                            value += math.log(aval)
                        else:
                            value += aval
                        
                    
                    elif data[solver][bench][bal][npart][seed]['SOLVED']==1:
                        count += 1
                        if geom:
                            aval = data[solver][bench][bal][npart][seed][stat]
                            if aval == 0:
                                aval = 0.000001
                            value += math.log(aval)
                        else:
                            value += data[solver][bench][bal][npart][seed][stat]
                            
    # print solver, count
    
    if count>0:
        # if stat=='OBJECTIVE' and count != len(benches):
        #     print solver, count, len(benches)
        #     return None
        # if count == 1:
        #     return int(value)
        # el
        if geom:
            return math.exp(value/count)
        else:
            return (value/count)
            
    return None
    
def get_solcount(index, data, solver, bal, benches):
    unsolved = 0
    subopt = 0
    for bench in benches:
        for npart in index.nparts:
            for seed in index.seeds:
                if data[solver][bench][bal][npart][seed] != None:
                    if data[solver][bench][bal][npart][seed]['SOLVED']==0:
                        unsolved += 1
                    elif data[solver][bench][bal][npart][seed]['OPTIMAL']==0:
                        subopt += 1
                else:
                    unsolved += 1

    return [unsolved, subopt]
    
def get_solved(index, data, solver, bal, bench):
    unsolved = 0
    subopt = 0
    for npart in index.nparts:
        for seed in index.seeds:
            if data[solver][bench][bal][npart][seed] != None:
                if data[solver][bench][bal][npart][seed]['SOLVED']==0:
                    unsolved += 1
                elif data[solver][bench][bal][npart][seed]['OPTIMAL']==0:
                    subopt += 1
            else:
                unsolved += 1

    return [unsolved, subopt]
    
   

def get_table(index, data, bal, stats, geom):
    table = Table(index.sorted_solvers,index.classes.keys())
    for bench_class in index.classes.keys():
        for solver in index.sorted_solvers:
            val = get_solcount(index, data, solver, bal, index.classes[bench_class])
            results = [get_mean(index, data, solver, bal, stat, index.classes[bench_class], geom) for stat in stats]
            val.extend(results)
            table += (solver,bench_class,val)
    return table
    
    
def get_big_table(index, data, bal, stats, geom):
  
    # benches = set([])
    table = Table(index.sorted_solvers,index.classes.keys())
    # for bench_class in index.classes.keys():
    #     for cl in index.classes[bench_class]:
          
    for cl in index.sorted_benches_ratio:
        # benches.add(cl)
        for solver in index.sorted_solvers:
            val = get_solcount(index, data, solver, bal, [cl])
            results = [get_mean(index, data, solver, bal, stat, [cl], geom) for stat in stats]
            val.extend(results)
            table += (solver,cl,val)
            
            
            print cl, solver, table[solver][cl]
                
                #
                # for npart in index.nparts:
                #     for seed in index.seeds:
                #         val = get_solved(index, data, solver, bal, cl)
                #         results = [data[solver][cl][bal][npart][seed][stat] for stat in stats]
                #         val.extend(results)
                #         table += (solver,cl.split('-')[1][:3],val)
                
                
    # print table
    
    # sorted_benches = list(benches)
    # sorted_benches.sort()
    
    return table
    


typeb = 'dimacs'

def parse():
    print 'index experiments',
    sys.stdout.flush()
    index = Index(default_keys)
    print 'ok'
    
    # print index.sorted_solvers
    # print index.classes
    

    print 'read data',
    sys.stdout.flush()
    data = read_data(index)
    print 'ok'

    # print index.classes

    index.compute_ratios(data)

    # for bclass in index.classes:
    #     print index.vcratio[bclass],
    #     for bench in index.classes[bclass]:
    #         print index.vcratio[bench],
    #     print
    
    return index, data




index, data = parse()

# print index.sorted_solvers

# balances = {}
# balances['snap'] = ['0.003', '0.005', '0.01']
# balances['dimacs'] = ['0', '4', '8']

# solvers = {}
# solvers['dimacs'] = ['decomp', 'vanilla_lex_rien', 'vanilla_lex_fil', 'witness_lex_midfil', 'witness_lex_midfilplus']
# solvers['snap'] = ['snap_decomp_lex', 'snap_vanilla_lex_rien', 'snap_crowns_lex', 'snap_wit_lex', 'snap_witplus_lex']
#




# tabfile = open('../../cp2016/tab%s.tex'%typeb, 'w')

# tabfile = open('../../cp2016/tabbvc.tex', 'w')
#
tabfile = open('./tabbvc.tex', 'w')

tabfile.write( index.head() )

for balance in ['tight', 'medium', 'loose']: #balances[typeb]:    
    print 'building table', balance
    sys.stdout.flush()
    table = get_table(index, data, balance, ['OBJECTIVE', 'BESTRUNTIME', 'BESTNODES'], False)
    tabfile.write( table.latex(index, balance, index.sorted_classes) )

tabfile.write( index.foot() )

tabfile.close()



tabfile = open('./taball.tex', 'w')

tabfile.write( index.head() )

for balance in ['tight', 'medium', 'loose']: #balances[typeb]:
    print 'building table', balance
    sys.stdout.flush()
    table = get_big_table(index, data, balance, ['OBJECTIVE', 'BESTRUNTIME', 'BESTNODES'], False)
    tabfile.write( table.latex(index, balance, index.sorted_benches_ratio) )

tabfile.write( index.foot() )

tabfile.close()


