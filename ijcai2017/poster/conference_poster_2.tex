%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dreuw & Deselaer's Poster
% LaTeX Template
% Version 1.0 (11/04/13)
%
% Created by:
% Philippe Dreuw and Thomas Deselaers
% http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\usetheme{I6pd2}
\usepackage[english]{babel}
\usepackage{amsmath,amsthm,amssymb,latexsym}
\usepackage{subfig}
\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{tikz}
\usepackage{tkz-berge}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,petri,shadows,calc,decorations.pathreplacing,matrix,shapes,topaths,fadings,mindmap,shapes.misc}

\graphicspath{{figures/}} % Location of the graphics files
\usepackage[rightcaption]{sidecap} % for caption positioning
\sidecaptionvpos{figure}{c}

\definecolor{carmine}{rgb}{0.59, 0.0, 0.09}
\definecolor{carminepink}{rgb}{0.92, 0.3, 0.26}
\definecolor{calpolypomonagreen}{rgb}{0.12, 0.3, 0.17}
\definecolor{mediumseagreen}{rgb}{0.24, 0.7, 0.44}
\definecolor{prussianblue}{rgb}{0.0, 0.19, 0.33}
\definecolor{bondiblue}{rgb}{0.0, 0.58, 0.71}
\definecolor{goldenbrown}{rgb}{0.6, 0.4, 0.08}
\definecolor{goldenyellow}{rgb}{1.0, 0.87, 0.0}
\definecolor{cadetblue}{rgb}{0.37, 0.62, 0.63}

\tikzset{VertexStyle/.style = {shape          = circle,
          ball color     = white, %airforceblue,
          text           = black,
          inner sep      = 2pt,
          outer sep      = 0pt,
          minimum size   = 10 pt,
					font           = \scriptsize}}
\tikzset{EdgeStyle/.style   = {very thick,
					shorten >       = 3pt, 
					shorten <       = 3pt}}
\tikzset{LabelStyle/.style =   {draw,
          fill           = blue!20!white,
          text           = black,
          font           = \scriptsize}}
	   
\tikzset{set/.style={draw,circle,ultra thick, inner sep=0pt,align=center}}
\tikzset{dashset/.style={draw,circle,dashed,ultra thick, inner sep=0pt,align=center}}
\tikzset{boxset/.style={draw,rectangle,ultra thick, inner sep=0pt,align=center}}
\tikzset{cross/.style={cross out, draw=black, minimum size=2*(#1-\pgflinewidth), inner sep=0pt, outer sep=0pt}, cross/.default={2pt}}

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{\huge On the Kernelization of Global Constraints}

\author{Cl\'ement Carbonnel$^1$ and Emmanuel Hebrard$^2$}

\institute{1: University of Oxford, United Kingdom - clement.carbonnel@cs.ox.ac.uk\\
2: LAAS-CNRS, France - hebrard@laas.fr}
\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks

\begin{frame}[t]

\begin{columns}[t]

\begin{column}{.02\textwidth}\end{column} % Empty spacer column

\begin{column}{.465\textwidth} % The first column

\begin{block}{Global constraints}

\vspace{6mm}

\begin{itemize}
\item \textbf{Global constraints} are (families of) predicates imposed on variables
\vspace{9mm}
\item Sometimes NP-hard
\begin{itemize}
\item Example: {\sc AtMostNValue}$(x_1,\ldots,x_n,U) \iff |\{x_1,\ldots,x_n\}| \leq U$
\end{itemize}
\vspace{9mm}
\item We consider NP-hard \textbf{minimization constraints}:
$$\pi(x_1,\ldots,x_n) \leq U, \; \; \pi \in \Pi$$
\vspace{9mm}
\item \emph{minimum cost}: minimum value of $\pi$ over all assignments
\vspace{9mm}
\item \emph{gap}: maximum of $U$ - minimum cost
\vspace{9mm}
\item \textbf{Propagation problem}: identify \textit{all} variables assignments that can be extended to a feasible tuple of the constraint
\vspace{9mm}
\end{itemize}
\end{block}

\vspace{5mm}

\begin{block}{Constraint kernelization}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
A \textbf{kernelization} is a polynomial-time algorithm that turns an instance $(x,k)$ of a parameterized decision problem into an equivalent instance $(x',k')$ whose size is a function of $k$ only.
\end{inblock}
\end{center}
\begin{itemize}
\item Can drastically reduce the problem size, topic subject to intense research
\end{itemize}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
\textbf{Question:} Can \emph{constraints} be kernelized?
\end{inblock}
\end{center}
\vspace{9mm}
\begin{itemize}
\item  Existing approach: turn the propagation problem into a number of decision problems and kernelize them
\begin{itemize}
\item Issues: redundancy + no clear-cut kernel
\end{itemize}
\end{itemize}

\vspace{9mm}

\begin{center}
\begin{tikzpicture}
\node[draw=black, thick, rectangle, line width=7pt] (cst) {$\pi(x_1,\ldots,x_n) \leq U, \pi \in \Pi$};
\node[draw=black, thick, rectangle, line width=7pt] (mid) [below=of cst, yshift=-20mm] {$\ldots$};
\node[draw=black, thick, rectangle, line width=7pt] (left) [left=of mid, xshift=-20mm] {$\exists t: \pi(t) \leq u \land t[x_1]=v_1$?};
\node[draw=black, thick, rectangle, line width=7pt] (right) [right=of mid, xshift=20mm] {$\exists t: \pi(t) \leq u \land t[x_n]=v_d$?};
\node[draw=black, thick, rectangle, line width=7pt] (kmid) [below=of mid, yshift=-30mm] {$\ldots$};
\node[draw=black, thick, rectangle, line width=7pt] (kleft) [below=of left, yshift=-20mm] {Kernel};
\node[draw=black, thick, rectangle, line width=7pt] (kright) [below=of right, yshift=-20mm] {Kernel};
\node[draw=black, thick, dashed, rectangle, line width=7pt] (res) [below=of kmid, yshift=-20mm] {consistent assignments to $x_1,\ldots,x_n,U$};

\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (cst.south) -- (left.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (cst.south) -- (mid.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (cst.south) -- (right.north);

\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (left.south) -- (kleft.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (mid.south) -- (kmid.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (right.south) -- (kright.north);

\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (kleft.south) -- (res.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (kmid.south) -- (res.north);
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (kright.south) -- (res.north);

%\node[draw=black, dashed, rectangle, rounded corners, line width=7pt] (ksupports) [below=of kernel, yshift=-12mm] {consistent assignments to $y_1,\ldots,y_{f(k)},U'$};
%\node[draw=black, dashed, rectangle, rounded corners, line width=7pt] (csupports) [below=of ksupports, yshift=-12mm] {consistent assignments to $x_1,\ldots,x_n,U$};
%\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (cst.south) -- (kernel.north) node [midway, right=5mm, fill=white] {Kernelization};
%\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (kernel.south) -- (ksupports.north) node [midway, right=5mm, fill=white] {Propagation (possibly exponential)};
%\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (ksupports.south) -- (csupports.north) node [midway, right=5mm, fill=white] {Polynomial-time computation};
\end{tikzpicture}
\end{center}

\vspace{9mm}
\end{block}

\vspace{5mm}

\begin{block}{Contribution}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
An alternative kernelization framework with \textbf{a single kernelization step}, which produces a smaller constraint whose propagation problem is polynomial-time equivalent.
\end{inblock}
\end{center}
\vspace{9mm}
\begin{center}
\hspace{70mm}\begin{tikzpicture}
\node[draw=black, thick, rectangle, line width=7pt] (cst) {$\pi(x_1,\ldots,x_n) \leq U, \pi \in \Pi$};
\node[draw=black, thick, rectangle, line width=7pt] (kernel) [below=of cst, yshift=-12mm] {$\pi'(y_1,\ldots,y_{f(k)}) \leq U', \pi' \in \Pi$};
\node[draw=black, dashed, rectangle, rounded corners, line width=7pt] (ksupports) [below=of kernel, yshift=-12mm] {consistent assignments to $y_1,\ldots,y_{f(k)},U'$};
\node[draw=black, dashed, rectangle, rounded corners, line width=7pt] (csupports) [below=of ksupports, yshift=-12mm] {consistent assignments to $x_1,\ldots,x_n,U$};
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (cst.south) -- (kernel.north) node [midway, right=5mm, fill=white] {Kernelization};
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (kernel.south) -- (ksupports.north) node [midway, right=5mm, fill=white] {Propagation (possibly exponential)};
\draw[style={-latex,>=stealth',thick,color=black, line width=7pt}] (ksupports.south) -- (csupports.north) node [midway, right=5mm, fill=white] {Polynomial-time computation};
\end{tikzpicture}
\end{center}
\vspace{9mm}
\end{block}

\vspace{5mm}

\end{column} % End of the first column

\begin{column}{.03\textwidth}\end{column} % Empty spacer column
 
\begin{column}{.465\textwidth} % The second column

\begin{block}{Our kernelization hierarchy}
\vspace{9mm}


\begin{center}
\begin{inblock}[40cm]{Theorem}
A \textbf{direct z-loss-less kernelization} maps a $\Pi$-constraint $C$ to a $\Pi$-constraint $C'$ of size $g(k)$ such that $C$ can be propagated in polynomial time provided that the outcome of propagation on $C'$ is known and the gap of $C$ is at most $z$.
\end{inblock}
\end{center}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
A \textbf{weak z-loss-less kernelization} is a direct z-loss-less kernelization such that non-kernel variables can be propagated in polynomial time if the minimum cost of $C$ is known and its gap is at most $z$.
\end{inblock}
\end{center}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
A \textbf{strong z-loss-less kernelization} is a weak z-loss-less kernelization such that non-kernel variables can be \emph{partially} propagated in polynomial time even if only a \emph{lower bound} to the minimum cost of $C$ is known.
\end{inblock}
\end{center}
%\begin{itemize}
%\item Sound polynomial-time propagation on NP-hard constraints with \emph{guaranteed convergence} towards completeness as lower bound quality increases!
%\end{itemize}

\vspace{9mm}

\begin{center}
\begin{tikzpicture}[scale=0.8,transform shape]
\node[boxset,fill=red!40,minimum height=22cm,text width=20cm] (nat) at (0,4)  (rea) {};
\node[dashset,fill=blue!40,text width=18cm] (int) at (0,4.5)  {};
\node[set,fill=olive!40,text width=14cm] (nat) at (0,6) {};
\node[set,fill=green!40,text width=10cm] (sm) at (0,7.2) {};
\node[set,fill=purple!70,text width=6cm] (core) at (0,8.4) {};

\node[] (ccsp) at (23,10.4) {\textbf{Classical kernel}};
\node[] (ccspnote) at (23,8.9) {\textbf{(decision problem only)}};
\node[] (csp) at (23,5) {\textbf{Direct loss-less kernel}};
\node[] (cspnote) at (23,3.5) {\textbf{(retains all information on propagation)}};
\node[] (nph) at (23,1.6) {\textbf{Weak loss-less kernel}};
\node[] (nphnote) at (23,0.1) {\textbf{(sound propagation if gap is known)}};
\node[] (sllk) at (23,-2) {\textbf{Strong loss-less kernel}};
\node[] (sllknote) at (23,-3.5) {\textbf{(sound propagation if gap is estimated)}};
\node[] (oi) at (23,-5.7) {\textbf{Original instance}};

\draw[ultra thick] (0,10.4) -- (ccsp);
\draw[ultra thick] (0,5) -- (csp);
\draw[ultra thick] (0,1.6) -- (nph);
\draw[ultra thick] (0,-2) -- (sllk);
\draw[ultra thick] (0,-5.7) -- (oi);
\end{tikzpicture}
\end{center}







\vspace{9mm}
\end{block}

\vspace{5mm}

\begin{block}{Application: The {\sc VertexCover} constraint}
\vspace{9mm}
\begin{itemize}
\item Constraint: Set $S$, $|S| \leq k$ must be a \textbf{vertex cover} of graph $G$
\item Parameter: $k$
\vspace{9mm}
\item Many known kernelizations techniques for the decision problem
\begin{itemize}
\item Theorem: the strongest ones are not even direct $0$-loss-less unless P=NP
\item Buss's Rule: size $k^2+k$, strong $\infty$-loss-less
\item ``Rigid crown'' reduction: size $2k$, strong $0$-loss-less
\item Anything inbetween?
\end{itemize}
\end{itemize}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
\textbf{Theorem:} For every $z$ there exists a strong $z$-loss-less kernelization of {\sc VertexCover} of size $(z+2)k$.
\end{inblock}
\end{center}
\begin{itemize}
\item Algorithm based on special types of crown decompositions
\end{itemize}
\vspace{9mm}
\begin{center}
    \begin{tikzpicture}[node distance   = 3.6 cm]
					%       \tikzset{VertexStyle/.style = {shape          = circle,
					%           ball color     = white, %airforceblue,
					%           text           = black,
					%           inner sep      = 2pt,
					%           outer sep      = 0pt,
					%           minimum size   = 10 pt,
					% font           = \scriptsize}}
					%       \tikzset{EdgeStyle/.style   = {very thick,
					% shorten >       = 3pt,
					% shorten <       = 3pt}}
					%       \tikzset{LabelStyle/.style =   {draw,
					%           fill           = blue!20!white,
					%           text           = black,
					%           font           = \scriptsize}}
      
      \node[VertexStyle](a){};
      \node[VertexStyle,right=of a](b){};
      \node[VertexStyle,right=of b](d){};
      \node[VertexStyle,below right=of a](e){};
      \node[VertexStyle,right=of e](f){};
			\node[VertexStyle,below left=of e](h){};
			\node[VertexStyle,right=of h](i){};
			\node[VertexStyle,right=of i](j){};
			\node[VertexStyle,right=of j](c){};
			\node[VertexStyle,below right=of h](k){};
			\node[VertexStyle,right=of k](l){};
			\node[VertexStyle,right=of l](m){};
      
      \draw[EdgeStyle](a) to  (e) ;
      \draw[EdgeStyle](b) to  (e) ;
      \draw[EdgeStyle](b) to  (f) ;
      \draw[EdgeStyle](c) to  (m) ;
      \draw[EdgeStyle](c) to  (k) ;
      \draw[EdgeStyle](d) to  (f) ;
			
      \draw[EdgeStyle](e) to  (f) ;
			\draw[EdgeStyle](e) to  (h) ;
			\draw[EdgeStyle](e) to  (k) ;
			
			\draw[EdgeStyle](f) to  (i) ;
			\draw[EdgeStyle](f) to  (j) ;

			\draw[EdgeStyle](h) to  (i) ;
			\draw[EdgeStyle](h) to  (k) ;
			
			\draw[EdgeStyle](i) to  (k) ;
			\draw[EdgeStyle](i) to  (l) ;
			
			\draw[EdgeStyle](j) to  (m) ;
			\draw[EdgeStyle](j) to  (l) ;
			
			\draw[EdgeStyle](l) to  (m) ;
			\draw[EdgeStyle](l) to  (k) ;
			
			
			\draw[rounded corners, thick, color=black] ([xshift=-5pt,yshift=5pt]a.north west) rectangle ([xshift=5pt,yshift=-5pt]d.south east);
			\node[right=5.9cm of d,color=black] {Independent set}; 
			
			
			\node[VertexStyle,ball color=white] at (a) {};
			\node[VertexStyle,ball color=white] at (b) {};
			\node[VertexStyle,ball color=white] at (d) {};
			
			\node[VertexStyle,ball color=carmine] at (e) {};
			\node[VertexStyle,ball color=carmine] at (f) {};
			
      \draw[EdgeStyle,color=black!10](a) to  (e) ;
      \draw[EdgeStyle,color=black!10](b) to  (e) ;
      \draw[EdgeStyle,color=black!10](b) to  (f) ;
      \draw[EdgeStyle,color=black!10](d) to  (f) ;
      \draw[EdgeStyle,color=black!10](e) to  (f) ;
			\draw[EdgeStyle,color=black!10](e) to  (h) ;
			\draw[EdgeStyle,color=black!10](e) to  (k) ;
			\draw[EdgeStyle,color=black!10](f) to  (i) ;
			\draw[EdgeStyle,color=black!10](f) to  (j) ;
			\draw[EdgeStyle,color=black!10](c) to  (f) ;
			
			\draw[rounded corners, thick, color=carmine] ([xshift=-5pt,yshift=5pt]e.north west) rectangle ([xshift=5pt,yshift=-5pt]f.south east);
			\node[right=7cm of f,color=carmine] (labelmid) {Belongs to \textit{all} minimum-cost vertex covers};
			\draw[->, thick, color=carmine, line width=5pt] ([xshift=-5pt]labelmid.west) -- ([xshift=5pt]f.east);
			
			\node[above right=.4cm and 3.2cm of m] {Residual graph $\approx$ kernel};
			
			\draw[rounded corners, thick, color=black] ([xshift=39pt,yshift=-5pt]m.south east) rectangle ([xshift=-5pt,yshift=5pt]h.north west);
			 
    \end{tikzpicture}
  \end{center}

\vspace{9mm}
\end{block}

\vspace{5mm}

\begin{block}{Application: The {\sc EdgeDominatingSet} constraint}
\vspace{9mm}
\begin{itemize}
\item Constraint: Set $S$, $|S| \leq k$ must be an \textbf{edge dominating set} of graph $G$
\item Parameter: k
\end{itemize}
\vspace{9mm}
\begin{center}
\begin{inblock}[40cm]{Theorem}
\textbf{Theorem:} Hagerup's $\max(6k, \frac{1}{2}k^2 + \frac{7}{2}k)$ kernelization for {\sc EdgeDominatingSet} is direct $\infty$-loss-less.
\end{inblock}
\end{center}
%\vspace{9mm}
%\begin{itemize}
%\item Not known to be weak/strong!
%\end{itemize}
\vspace{9mm}
\end{block}

\vspace{5mm}

%\begin{block}{Perspectives}
%\vspace{9mm}
%\begin{itemize}
%\item Extend the concept to other global constraints
%\begin{itemize}
%\item {\sc NValue} with k = number of holes is a prime candidate
%\end{itemize}
%\item Run more experiments
%\begin{itemize}
%\item Promising results on {\sc VertexCover}
%\end{itemize}
%\end{itemize}
%\vspace{9mm}
%\end{block}

\vspace{5mm}

%\nocite{*} % Insert publications even if they are not cited in the poster
%\small{\bibliographystyle{unsrt}
%\bibliography{sample}}

%----------------------------------------------------------------------------------------

\end{column} % End of the second column

\begin{column}{.015\textwidth}\end{column} % Empty spacer column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}