tablefile = open('tabbvc.tex', 'r')


tablefile.readline()
tablefile.readline()

solverline = tablefile.readline().split('&')[1:]

solvers = [s[s.rfind('{')+1:s.rfind('}')] for s in solverline]

data = {}.fromkeys(solvers)
for solver in solvers:
  data[solver] = {}

stats = ['\#s', 'gap', 'cpu', '\#nd']
types = ['balancing constraint: tight', 'balancing constraint: medium', 'balancing constraint: loose']

tablefile.readline()


lines = tablefile.read().split('\\\\')


def decolor(w):
  n = w
  if w.find("cellcolor")>=0 :
    n = w[w.rfind('{')+1:w.rfind('}')]
  ik = n.find("K")
  if ik>=0 :
    return float(n[:ik])*1000
  im = n.find("M")
  if im>=0 :
    return float(n[:im])*1000000
  return float(n)
  
  
num_instance = []

tightness = None
for line in lines:
  if line.find('balancing') >= 0 :
    tightness = line[line.rfind('{')+1:line.rfind('}')]
    for solver in solvers:
      data[solver][tightness] = {}.fromkeys(stats)
      for stat in stats:
        data[solver][tightness][stat] = []
  else:
    dline = line.replace('\\hline', '').split('&')
    row = [decolor(w) for w in dline[2:]]
    if len(row) == len(solvers)*len(stats) :
      if len(num_instance)<15 :
        num_instance.append(int(dline[0]))
      i = 0
      for solver in solvers:
        for stat in stats:
          data[solver][tightness][stat].append(row[i])
          i += 1
          
          
def avg(l):
  return round(sum([float(x) for x in l])/float(len(l)),2)
        
         
# print data

# print num_instance
pretty = {}.fromkeys(stats)
pretty['\#s'] = '\\# unsolved instances'
pretty['gap'] = 'Gap w.r.t. min vertex cover'
pretty['cpu'] = 'CPU time'


for stat in stats[:-1]:
  print '\n\\begin{frame}[fragile]\n\\frametitle{%s}\n\\begin{columns}[c]\n\\column{.25\\textwidth}\n\\explegend\n\\column{.75\\textwidth}'%(pretty[stat])
  print "\\histo{%s}{%s}"%(', '.join(solvers),pretty[stat]),
  for tightness in types:
    print '{',
    for solver in solvers:
      print "(%s,%.2f)"%(solver,avg([x/n for x,n in zip(data[solver][tightness][stat],num_instance)])),
    print '}',
  print '\n\\end{columns}\n\\end{frame}\n'
  
  
ratios = [range(6),range(6,8),range(9,13),range(13,15)]  
  
for stat in stats[:-1]:
  print '\n\\begin{frame}[fragile]\n\\frametitle{%s}\n\\begin{columns}[c]\n\\column{.25\\textwidth}\n\\explegendg\n\\column{.75\\textwidth}'%(pretty[stat])
  print "\\histog{%s}{%s}"%(', '.join(solvers),pretty[stat]),
  for ratio in ratios:
    print '{',
    for solver in solvers:
      vals = []
      for tightness in types:
        for r in ratio:
          vals.append(data[solver][tightness][stat][r]/num_instance[r])
      print "(%s,%.2f)"%(solver,avg(vals)),
    print '}',
  print '\n\\end{columns}\n\\end{frame}\n'
  



